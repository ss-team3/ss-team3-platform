import '../styles/globals.css'
import { useEffect } from 'react'

import HeadInfo from '../components/HeadInfo'
import AuthProvider from '../components/providers/AuthProvider'

import { getCurrentUser } from '../api/user'

function MyApp ({ Component, pageProps }) {
  const preGetUser = async () => {
    const user = await getCurrentUser()
    setTheme('light', user.theme)
  }
  const setTheme = (originTheme, newTheme) => {
    const root = window.document.documentElement
    root.classList.remove(originTheme)
    root.classList.add(newTheme)
  }
  useEffect(() => {
    preGetUser()
  }, [])

  return (
    <div>
      <HeadInfo />
      <AuthProvider>
        <Component {...pageProps} />
      </AuthProvider>
    </div>
  )
}

export default MyApp
