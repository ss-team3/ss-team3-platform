import { Component } from 'react'
import Router from 'next/router'

import CategorySettingList from '../../components/CategorySettingList'
import CategoryAddCard from '../../components/CategoryAddCard'
import HeaderBar from '../../components/HeaderBar'
import NavBar from '../../components/NavBar'
import Layout from '../../components/Layout'
import Loading from '../../components/Loading'
import Modal from '../../components/Modal'
import { AuthContext } from '../../components/providers/AuthProvider'

import {
  addCurrentCategory,
  getCurrentCategories,
  modifyCurrentCategory,
  removeCurrentCategory,
} from '../../api/category'

export default class CategoriesSetting extends Component {
  constructor (props) {
    super(props)
    this.state = {
      categories: { total: 0, data: [] },
      shouldDisplayModal: false,
      modalContent: '',
      warningIcon: false,
      leftButton: '',
      rightButton: '',
      pendingDeleteId: '',
    }
    this.handleCategoryRename = this.handleCategoryRename.bind(this)
    this.handleCategoryAdd = this.handleCategoryAdd.bind(this)
  }

  handleCategoryRename (_id, name, url = '') {
    const { categories } = this.state
    const { data } = categories
    let idx = null

    for (const [index, datum] of data.entries()) {
      if (datum._id === _id) {
        if (name === datum.name) {
          return false
        }
        idx = index
      } else {
        if (!name) {
          this.setState({
            modalContent: 'Cannot use empty category name',
            warningIcon: false,
            shouldDisplayModal: true,
            rightButton: 'OK!',
            leftButton: '',
          })
          return
        }

        if (['quick', 'short', 'long', 'focus', 'bookmark', 'all'].includes(name.toLowerCase())) {
          this.setState({
            modalContent: 'Cannot use reserved category name',
            warningIcon: false,
            shouldDisplayModal: true,
            rightButton: 'OK!',
            leftButton: '',
          })
          return
        }
        if (name === datum.name) {
          this.setState({
            modalContent: 'the category is already exist',
            warningIcon: false,
            shouldDisplayModal: true,
            rightButton: 'OK!',
            leftButton: '',
          })
          return false
        }
      }
    }

    this.setState((prevState) => {
      const categories = prevState.categories
      categories.data[idx].name = name
      return { categories: categories }
    }, async () => {
      await modifyCurrentCategory({ _id, name })
    })
    return true
  }

  handleModalButtonClick = () => {
    this.setState({ shouldDisplayModal: false, pendingDeleteId: '' })
  }

  handleDeleteButtonClick = (_id) => {
    this.setState({
      shouldDisplayModal: true,
      modalContent: 'Make sure you want to delete this.',
      warningIcon: true,
      leftButton: 'Delete',
      rightButton: 'Cancel',
      pendingDeleteId: _id,
    })
  }

  handleCategoryRemoveConfirm = () => {
    const _id = this.state.pendingDeleteId
    this.setState((prevState) => {
      const data = prevState.categories.data.filter(i => i._id !== _id)
      const total = prevState.categories.data.length - data.length
      return {
        shouldDisplayModal: false,
        categories: { total, data },
        pendingDeleteId: '',
      }
    },
    async () => {
      await removeCurrentCategory({ _id })
    })
  }

  handleCategoryAdd (name) {
    const { categories } = this.state
    const { data } = categories

    if (!name) {
      this.setState({
        modalContent: 'Cannot use empty category name',
        warningIcon: false,
        shouldDisplayModal: true,
        rightButton: 'OK!',
        leftButton: '',
      })
      return
    }

    if (['quick', 'short', 'long', 'focus', 'bookmark', 'All'].includes(name.toLowerCase())) {
      this.setState({
        modalContent: 'Cannot use reserved category name',
        warningIcon: false,
        shouldDisplayModal: true,
        rightButton: 'OK!',
        leftButton: '',
      })
      return
    }

    if (data.find(i => i.name === name)) {
      this.setState({
        modalContent: 'the category is already exist',
        warningIcon: false,
        shouldDisplayModal: true,
        rightButton: 'OK!',
        leftButton: '',
      })
      return
    }

    const tempId = `temp-${Date.now()}`
    this.setState((prevState) => {
      const categories = prevState.categories
      categories.data.push({
        name: name,
        avatar_url: '',
        // dummy data
        _id: tempId,
        articles: [],
      })
      return { categories: categories }
    }, async () => {
      const { _id: rightId } = await addCurrentCategory({ name, avatar_url: '' })
      this.setState((prevState) => {
        const newCategoryData = prevState.categories.data.map(i => {
          if (i._id === tempId) i._id = rightId
          return i
        })
        return { categories: { total: prevState.categories.total, data: newCategoryData } }
      })
    })
  }

  async initAPI () {
    const res = await getCurrentCategories()
    this.setState({ categories: res })
  }

  async componentDidMount () {
    await this.initAPI()
  }

  render () {
    const { isLogin } = this.context
    if (!isLogin) {
      if (isLogin === false) { Router.push('/login') }
      return (<Loading />)
    }

    const { categories, shouldDisplayModal, modalContent, rightButton, leftButton, warningIcon } = this.state

    const modal = (shouldDisplayModal
      ? (
        <div className='w-80 max-w-full fixed left-0 right-0 m-auto'>
          <div className='mx-4 rounded-lg shadow-lg bg-primary text-primary'>
            <Modal
              warningIcon={warningIcon}
              content={modalContent}
              rightButton={rightButton}
              leftButton={leftButton}
              onRightButtonClick={this.handleModalButtonClick}
              onLeftButtonClick={this.handleCategoryRemoveConfirm}
            />
          </div>
        </div>
        )
      : <></>)

    return (
      <>
        <HeaderBar title='Categories Setting' canNavBack />
        <Layout shouldPadHeaderBar shouldPadNavBar>
          {modal}
          <div className='flex flex-col items-center gap-4'>
            <CategorySettingList
              categories={categories}
              onCategoryRename={this.handleCategoryRename}
              onCategoryRemove={this.handleDeleteButtonClick}
            />
            <CategoryAddCard onCategoryAdd={this.handleCategoryAdd} />
          </div>
        </Layout>
        <NavBar />
      </>
    )
  }
}

CategoriesSetting.contextType = AuthContext
