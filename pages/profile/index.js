import { Component } from 'react'
import Router from 'next/router'

import Button from '../../components/Button'
import NavBar from '../../components/NavBar'
import Avatar from '../../components/Avatar'
import ProfileDataList from '../../components/ProfileDataList'
import ProfileCategoryList from '../../components/ProfileCategoryList'
import ProfileThemeSettingCard from '../../components/ProfileThemeSettingCard'
import ProfileTargetSettingCard from '../../components/ProfileTargetSettingCard'
import Layout from '../../components/Layout'
import Loading from '../../components/Loading'
import Modal from '../../components/Modal'
import { AuthContext } from '../../components/providers/AuthProvider'

import { signOut } from '../../lib/firebase'
import { getCurrentUser, modifyCurrentUser } from '../../api/user'
import { getCurrentPosts } from '../../api/article'
import { getCurrentCategories } from '../../api/category'

export default class Profile extends Component {
  constructor (props) {
    super(props)
    this.state = {
      user: {},
      categories: { total: 0, data: [] },
      numOfBookmark: 0,
      numOfTarget: 0,
      shouldDisplayModal: false,
    }
    this.initAPI = this.initAPI.bind(this)
    this.handleThemeChange = this.handleThemeChange.bind(this)
    this.handleTargetChange = this.handleTargetChange.bind(this)
  }

  async initAPI () {
    const [userData, postsData, categoriesData] = await Promise.all([getCurrentUser(), getCurrentPosts(), getCurrentCategories()])
    const { data: posts } = postsData
    const { target_read_article: numOfTarget } = userData
    const numOfBookmark = posts.filter(i => { return i.bookmark === true }).length
    this.setState({
      user: userData,
      categories: categoriesData,
      numOfBookmark: numOfBookmark,
      numOfTarget: numOfTarget,
    })
  }

  async componentDidMount () {
    await this.initAPI()
  }

  async handleThemeChange (theme) {
    const setTheme = (originTheme, newTheme) => {
      const root = window.document.documentElement
      root.classList.remove(originTheme)
      root.classList.add(newTheme)
    }

    this.setState((prevState) => {
      const user = { ...prevState.user }
      setTheme(user.theme, theme)
      user.theme = theme
      return { user }
    }, async () => {
      await modifyCurrentUser({ theme })
    })
  }

  handleModalButtonClick = () => {
    this.setState({ shouldDisplayModal: false })
  }

  async handleTargetChange (target) {
    if (isNaN(Number(target)) || Number(target) <= 0) {
      this.setState({ shouldDisplayModal: true })
      return
    }
    const setTarget = (originTarget, newTarget) => {
      const root = window.document.documentElement
      root.classList.remove(originTarget)
      root.classList.add(newTarget)
    }
    this.setState((prevState) => {
      const user = { ...prevState.user }
      setTarget(user.target_read_article, target)
      user.target_read_article = Number(target)
      return { user }
    }, async () => {
      await modifyCurrentUser({ target_read_article: Number(target) })
    })
  }

  render () {
    const { isLogin, userAuth } = this.context
    if (!isLogin) {
      if (isLogin === false) { Router.push('/login') }
      return (<Loading />)
    }

    const { photoUrl = '' } = userAuth
    const { user, categories, numOfBookmark, shouldDisplayModal } = this.state
    const {
      target_read_article: targetReadArticle,
      total_read_time: totalReadTime,
      week_already_read: weekAlreadyRead,
    } = user

    return (
      <>
        <Layout shouldFullScreen shouldPadNavBar>
          <div className={`w-80 max-w-full fixed left-0 right-0 m-auto ${shouldDisplayModal ? 'block' : 'hidden'}`}>
            <div className='mx-4 rounded-lg shadow-lg bg-primary text-primary'>
              <Modal
                warningIcon={false}
                content='目標需要是正整數'
                rightButton='OK!'
                leftButton=''
                onRightButtonClick={this.handleModalButtonClick}
              />
            </div>
          </div>
          <div className='flex flex-col justify-center items-center gap-4'>
            <div className='flex flex-col items-center'>
              <Avatar type='image' size='w-20 h-20 md:w-32 md:h-32' url={photoUrl} />
              {user.name}
            </div>
            <ProfileDataList
              totalReadTime={totalReadTime}
              targetReadArticle={targetReadArticle}
              weekAlreadyRead={weekAlreadyRead}
              numOfBookmark={numOfBookmark}
            />
            <div className='w-full md:w-3/4'>
              {/* <div className='w-full'> */}
              <ProfileCategoryList categories={categories} />
              <div className='w-full flex flex-row items-center justify-between'>
                <p className='text-xl font-bold'>Settings</p>
              </div>
              <ProfileThemeSettingCard
                onChangeTheme={this.handleThemeChange}
                theme={user.theme}
              />
              <ProfileTargetSettingCard
                onChangeTarget={this.handleTargetChange}
                targetRead={user.target_read_article}
                alreadyRead={user.week_already_read}
              />
            </div>
            <Button
              onClick={() => { signOut(); Router.push('/login') }}
              textColor='text-white'
              bgColor='bg-secondary hover:bg-gray-500 mt-6'
              className='hover:shadow-inner text-xl font-semibold'
            >Sign Out
            </Button>
          </div>
        </Layout>
        <NavBar />
      </>
    )
  }
}

Profile.contextType = AuthContext
