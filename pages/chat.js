import { Component } from 'react'
import Router from 'next/router'

import Loading from '../components/Loading'
import Layout from '../components/Layout'
import MessageList from '../components/MessageList'
import PetHeaderBar from '../components/PetHeaderBar'
import MessageInput from '../components/MessageInput'
import { AuthContext } from '../components/providers/AuthProvider'

import { getCurrentPet } from '../api/pet'
import { addCurrentMessage, getCurrentMessages } from '../api/message'

export default class ChatPage extends Component {
  constructor (props) {
    super(props)
    this.state = {
      pet: {},
      messages: [],
    }
    this.initAPI = this.initAPI.bind(this)
    this.handleChatInputClick = this.handleChatInputClick.bind(this)
    this.getHumanSayings = this.getHumanSayings.bind(this)
  }

  async initAPI () {
    const pet = await getCurrentPet()
    const { data: messages } = await getCurrentMessages({ limit: 10, order: [{ sent_at: -1 }] })
    this.setState({ pet, messages })
  }

  async componentDidMount () {
    await this.initAPI()
  }

  componentDidUpdate () {
    const element = document.getElementById('bottom')
    if (element !== null) element.scrollIntoView({ block: 'end', behavior: 'smooth' })
  }

  handleArticleClick (_id) {
    Router.push(`/article/${_id}`)
  }

  async handleChatInputClick (category) {
    const content = this.getHumanSayings(category)
    await addCurrentMessage({ type: 'message', content: content, send_to_pet: true })

    if (category === 'recommend') {
      await addCurrentMessage({ type: 'message', content: '你要哪種文章 U・x・U', send_to_pet: false })
    } else {
      let filter
      switch (category) {
        case 'quick':
          filter = { estimated_time: { $lt: 5 } }
          break
        case 'short':
          filter = { estimated_time: { $gte: 5, $lt: 10 } }
          break
        case 'long':
          filter = { estimated_time: { $gte: 10, $lt: 20 } }
          break
        case 'focus':
          filter = { estimated_time: { $gte: 20 } }
          break
      }
      await addCurrentMessage({ article_filter: filter, type: 'article', content: '', send_to_pet: false })
    }
    const { data: newMessages } = await getCurrentMessages({ limit: 5, order: [{ sent_at: -1 }] })
    this.setState((prevState) => {
      const originMessageIds = prevState.messages.map(m => m._id)
      // filter duplicate message & concat
      const messages = [...prevState.messages].concat(newMessages.filter(m => !originMessageIds.includes(m._id)))

      return { messages }
    })
  }

  getHumanSayings (category) {
    const { name } = this.state.pet
    if (category === 'recommend') {
      return `${name}推薦我一篇文章<(_ _)>`
    } else if (category === 'quick') {
      return '希望能快一點看完><'
    } else if (category === 'short') {
      return '短一點的就好^_^'
    } else if (category === 'long') {
      return '今天好像可以看久一點^w^/'
    } else if (category === 'focus') {
      return '我想看超長的文章!>w<!'
    }
  }

  render () {
    const { isLogin } = this.context
    if (!isLogin) {
      if (isLogin === false) { Router.push('/login') }
      return (<Loading />)
    }

    const { pet, messages } = this.state
    return (
      <>
        <PetHeaderBar pet={this.state.pet} />
        <Layout shouldPadHeaderBar shouldPadNavBar>
          <MessageList
            id='messageList'
            pet={pet} messages={messages}
            onArticleClick={this.handleArticleClick}
          />
        </Layout>
        <MessageInput onClick={this.handleChatInputClick} />
        <div id='bottom' />
      </>
    )
  }
}

ChatPage.contextType = AuthContext
