import { Component } from 'react'
import Router from 'next/router'

import CategoryList from '../../components/CategoryList'
import CategoryCardList from '../../components/CategoryCardList'
import DefaultCategoryList from '../../components/DefaultCategoryList'
import NavBar from '../../components/NavBar'
import Layout from '../../components/Layout'
import Loading from '../../components/Loading'
import { AuthContext } from '../../components/providers/AuthProvider'

import { getCurrentCategories } from '../../api/category'
import { getCurrentPosts } from '../../api/article'

export default class CategoryIndex extends Component {
  constructor (props) {
    super(props)
    this.state = {
      categories: {
        total: 0,
        data: [],
      },
      posts: {
        total: 0,
        data: [],
      },
    }
  }

  async initAPI () {
    const res = await getCurrentCategories()
    const posts = await getCurrentPosts()
    this.setState({ categories: res })
    this.setState({ posts })
  }

  async componentDidMount () {
    await this.initAPI()
  }

  render () {
    const { isLogin } = this.context
    const { categories, posts } = this.state
    if (!isLogin) {
      if (isLogin === false) { Router.push('/login') }
      return (<Loading />)
    }

    return (
      <>
        <Layout shouldPadNavBar>
          <div className='flex flex-col items-center gap-4 bg-primary'>
            <CategoryCardList />
            <CategoryList categories={categories} />
            <DefaultCategoryList posts={posts} />
          </div>
        </Layout>
        <NavBar />
      </>
    )
  }
}

CategoryIndex.contextType = AuthContext
