import { Component } from 'react'
import Router from 'next/router'
import _ from 'lodash'

import HeaderBar from '../../components/HeaderBar'
import Loading from '../../components/Loading'
import ArticleList from '../../components/ArticleList'
import Layout from '../../components/Layout'
import { AuthContext } from '../../components/providers/AuthProvider'

import { getCurrentPosts, modifyCurrentArticle } from '../../api/article'
import { getCurrentCategory } from '../../api/category'

class Category extends Component {
  constructor (props) {
    super(props)
    this.state = {
      total: 0,
      data: [],
      title: '',
    }

    this.initAPI = this.initAPI.bind(this)
    this.handleChangeBookmarkArchived = this.handleChangeBookmarkArchived.bind(this)
  }

  async initAPI () {
    const { id } = this.props
    let res = null
    let title = id

    if (id === 'quick') res = await getCurrentPosts({ filter: { estimated_time: { $lt: 5 } } })
    else if (id === 'short') res = await getCurrentPosts({ filter: { estimated_time: { $gte: 5, $lt: 10 } } })
    else if (id === 'long') res = await getCurrentPosts({ filter: { estimated_time: { $gte: 10, $lt: 20 } } })
    else if (id === 'focus') res = await getCurrentPosts({ filter: { estimated_time: { $gte: 20 } } })
    else if (id === 'All') res = await getCurrentPosts()
    else if (id === 'bookmark') res = await getCurrentPosts({ filter: { bookmark: true } })
    else {
      res = await getCurrentPosts({ filter: { category_id: id } })
      const category = await getCurrentCategory({ _id: id })
      title = category.name
    }

    if (['quick', 'short', 'long', 'focus'].includes(id)) {
      title = `${title[0].toUpperCase()}${title.slice(1)}`
    }

    // sort posts
    const total = res.total
    const data = _.orderBy(res.data, ['estimated_time'], ['asc'])

    this.setState({ title, total, data })
  }

  async componentDidMount () {
    await this.initAPI()
  }

  handleChangeBookmarkArchived (_id, bookmark = null, archived = null) {
    const newData = [...this.state.data]
    const idx = newData.findIndex(i => i._id === _id)
    if ((bookmark === null && archived === null) ||
       (bookmark === newData[idx].bookmark && archived === newData[idx].archived)) return

    if (bookmark !== null) newData[idx].bookmark = bookmark
    if (archived !== null) newData[idx].archived = archived

    this.setState(prevState => {
      return { total: prevState.total, data: newData }
    }, async () => {
      await modifyCurrentArticle({ _id, bookmark: newData[idx].bookmark, archived: newData[idx].archived })
    })
  }

  render () {
    const { isLogin } = this.context
    if (!isLogin) {
      if (isLogin === false) { Router.push('/login') }
      return (<Loading />)
    }

    // TODO: using pagination
    const { id } = this.props
    const { total, data, title } = this.state
    return (
      <>
        <HeaderBar title={title} canNavBack />
        <Layout shouldPadHeaderBar>
          <ArticleList
            categoryId={id}
            total={total}
            data={data}
            onChangeBookmarkArchived={this.handleChangeBookmarkArchived}
          />
        </Layout>
      </>
    )
  }
}

Category.contextType = AuthContext

export default Category

export async function getStaticPaths () {
  /**
   * Since we deploy frontend and backend at the same time,
   * the following api call fails to reach the backend.
   * Thus we can't get the id when building
   */
  // const res = await getCategoriesForAdmin()
  // const listIDs = res.data.map(category => category._id)
  // const cardIDs = ['quick', 'short', 'long', 'focus']
  // const ids = listIDs.concat(cardIDs)

  const defaultIds = ['quick', 'short', 'long', 'focus', 'bookmark', 'All']

  // Return a list of possible value for id
  const paths = defaultIds.map((id) => ({ params: { id } }))
  return {
    paths,
    fallback: true,
  }
}

export async function getStaticProps ({ params }) {
  // Fetch necessary data for the blog post using params.id
  const { id } = params
  return {
    props: {
      id,
    },
  }
}
// tutorial: https://nextjs.org/learn/basics/dynamic-routes/page-path-external-data
// doc: https://nextjs.org/docs/routing/dynamic-routes
