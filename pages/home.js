import { Component } from 'react'
import Link from 'next/link'
import Router from 'next/router'

import PostList from '../components/PostList'
import Layout from '../components/Layout'
import HeaderBar from '../components/HeaderBar'
import PetBar from '../components/PetBar'
import Icon from '../components/Icon'
import Loading from '../components/Loading'
import NavBar from '../components/NavBar'
import { AuthContext } from '../components/providers/AuthProvider'

import { getCurrentUser } from '../api/user'
import { getCurrentPet } from '../api/pet'

class Home extends Component {
  constructor (props) {
    super(props)
    this.state = {
      user: {},
      pet: {},
    }
  }

  async initAPI () {
    const [user, pet] = await Promise.all([getCurrentUser(), getCurrentPet()])
    this.setState({ user, pet })
  }

  async componentDidMount () {
    await this.initAPI()
  }

  render () {
    const { isLogin } = this.context
    if (!isLogin) {
      if (isLogin === false) { Router.push('/login') }
      return (<Loading />)
    }

    const { pet, user } = this.state

    const addIcon = (
      <Link href='/article/add' key='add'>
        <a>
          <Icon type='add' textColor='text-secondary' textSize='text-3xl' />
        </a>
      </Link>
    )
    const chatIcon = (
      <Link href='/chat' key='chat'>
        <a>
          <Icon type='chat' textColor='text-secondary' textSize='text-3xl' />
        </a>
      </Link>
    )
    const petBar = (
      <PetBar
        pet={pet}
        targetRead={user.target_read_article}
        alreadyRead={user.week_already_read}
      />
    )
    const headerBarUtils = [addIcon, chatIcon]

    return (
      <div>
        <HeaderBar title='Product' rightUtils={headerBarUtils} />
        <Layout shouldPadHeaderBar shouldPadNavBar>
          <PostList
            topContent={petBar}
          />
        </Layout>
        <NavBar />
      </div>
    )
  }
}

Home.contextType = AuthContext

export default Home
