import { Component } from 'react'
import Router from 'next/router'

import ProductCollections from '../components/store/ProductCollections'
import HeaderBar from '../components/HeaderBar'
import Icon from '../components/Icon'
import Layout from '../components/Layout'
import Modal from '../components/Modal'
import Loading from '../components/Loading'
import { AuthContext } from '../components/providers/AuthProvider'

import { getCurrentUser } from '../api/user'

export default class Store extends Component {
  constructor (props) {
    super(props)
    this.state = {
      user: {},
      shouldDisplayModal: false,
      modalContent: '',
    }
    this.initAPI = this.initAPI.bind(this)
    this.handleChangeUserMoney = this.handleChangeUserMoney.bind(this)
  }

  async initAPI () {
    const userData = await getCurrentUser()
    this.setState({
      user: userData,
    })
  }

  async handleChangeUserMoney () {
    const userData = await getCurrentUser()
    this.setState({
      user: userData,
    })
  }

  handleModal =(content) => {
    this.setState({
      shouldDisplayModal: true,
      modalContent: content,
    })
  }

  handleModalButtonClick = () => {
    this.setState({ shouldDisplayModal: false })
  }

  async componentDidMount () {
    await this.initAPI()
  }

  render () {
    const { isLogin } = this.context
    if (!isLogin) {
      if (isLogin === false) { Router.push('/login') }
      return (<Loading />)
    }

    const { shouldDisplayModal, modalContent, user } = this.state
    const { money = 0 } = user

    const modal = shouldDisplayModal
      ? (
        <div className='w-80 max-w-full fixed left-0 right-0 m-auto'>
          <div className='mx-4 rounded-lg shadow-lg bg-primary text-primary'>
            <Modal
              warningIcon={false}
              content={modalContent}
              rightButton='OK!'
              leftButton=''
              onRightButtonClick={this.handleModalButtonClick}
            />
          </div>
        </div>
        )
      : <></>

    return (
      <>
        <HeaderBar
          title='Store' canNavBack
          rightUtils={
            <div className='text-secondary flex items-center m-1'>
              <Icon type='money' textSize='text-2xl' />
              {money}
            </div>
          }
        />
        <Layout shouldPadHeaderBar>
          {modal}
          <ProductCollections
            userMoney={money}
            onChangeUserMoney={this.handleChangeUserMoney}
            onHandleModal={this.handleModal}
          />
        </Layout>
      </>
    )
  }
}

Store.contextType = AuthContext
