import Router from 'next/router'

export default function Custom404 () {
  return (
    <div className='w-screen h-screen flex flex-col justify-between items-center'>
      <div className='w-full h-full text-6xl md:text-9xl text-secondary flex flex-col justify-center items-center'>
        <div className='flex w-full justify-center items-end gap-4 md:gap-7'>
          <div className='pb-11 sm:pb-20 md:pb-32'>
            4
          </div>
          <img src='/pet/sleepy_cat.webp' className='h-40 sm:h-72 md:h-auto' />
          <div className='pb-11 sm:pb-20 md:pb-32'>
            4
          </div>
        </div>
        <div className='text-2xl md:text-5xl pb-2 md:pb-4'>
          Wow, how do you get here!
        </div>
        <div className='text-xl md:text-3xl'>
          Let me get you back to
          <span onClick={() => Router.push('/home')} className='cursor-pointer text-primary'> home</span>.
        </div>
      </div>
      <div className='mb-4 text-base text-primary'>
        Powered by Pokcat team
      </div>
    </div>
  )
}
