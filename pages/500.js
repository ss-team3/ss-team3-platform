import Router from 'next/router'

export default function Custom500 () {
  return (
    <div className='w-screen h-screen flex flex-col justify-between items-center'>
      <div className='w-full h-full text-6xl md:text-9xl text-secondary flex flex-col justify-center items-center'>
        <div className='flex w-full justify-center items-end gap-3 md:gap-6 pb-11'>
          <div className='pb-1 md:pb-3'>
            5
          </div>
          <img src='/logo.png' className='h-16 md:h-36' />
          <img src='/logo.png' className='h-16 md:h-36' />
        </div>
        <div className='text-2xl md:text-6xl pb-2 md:pb-4'>
          Oops, server error...
        </div>
        <div className='text-xl md:text-3xl'>
          My bad! Please let me
          <span onClick={() => Router.reload()} className='cursor-pointer text-primary'> try again</span>.
        </div>
      </div>
      <footer className='mb-4 text-base text-primary'>
        Powered by Pokcat team
      </footer>
    </div>
  )
}
