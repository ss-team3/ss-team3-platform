import { getPosts } from '../../../lib/serverSide/api'

export default async function (req, res) {
  const data = await getPosts()
  res.json(data)
}
