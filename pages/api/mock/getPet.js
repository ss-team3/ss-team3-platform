import { getPet } from '../../../lib/serverSide/api'

export default async function (req, res) {
  const response = await getPet()
  const data = response
  res.json(data)
}
