import { getUser } from '../../../lib/serverSide/api'

export default async function (req, res) {
  const response = await getUser()
  const data = response
  res.json(data)
}
