import { getArticles } from '../../../lib/serverSide/api'

export default async function (req, res) {
  const response = await getArticles()
  res.json(response)
}
