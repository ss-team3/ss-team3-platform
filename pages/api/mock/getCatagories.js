import { getCategories } from '../../../lib/serverSide/api'

export default async function (req, res) {
  const data = await getCategories()
  res.json(data)
}
