import { getOwnedProducts } from '../../../lib/serverSide/api'

export default async function (req, res) {
  const response = await getOwnedProducts()
  const data = response
  res.json(data)
}
