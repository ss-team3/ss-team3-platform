import { getArticles } from '../../../lib/serverSide/api'

export default async function (req, res) {
  const data = await getArticles()
  res.json(data)
}
