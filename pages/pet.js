import { Component } from 'react'
import Link from 'next/link'
import Router from 'next/router'

import Icon from '../components/Icon'
import PetName from '../components/PetName'
import PetStatus from '../components/PetStatus'
import Pet from '../components/Pet'
import NavBar from '../components/NavBar'
import OwnedProductList from '../components/OwnedProductList'
import Modal from '../components/Modal'
import Layout from '../components/Layout'
import HeaderBar from '../components/HeaderBar'
import Loading from '../components/Loading'
import { AuthContext } from '../components/providers/AuthProvider'

import { getCurrentUser } from '../api/user'
import { getCurrentPet, modifyCurrentPet } from '../api/pet'
import { getOwnedProducts, useOwnedProduct } from '../api/ownedProduct'

export default class PetPage extends Component {
  constructor (props) {
    super(props)
    this.state = {
      user: { money: 0 },
      pet: { hunger: 0 },
      // To keep `pet` consist with backend data, `displayedPetName` used to display un-saved pet name
      displayedPetName: '',
      ownedProducts: [],
      saying: '...',
      shouldDisplayModal: false,
      modalContent: '',
    }

    this.handleUseOwnedProduct = this.handleUseOwnedProduct.bind(this)
    this.handlePetNameOnChange = this.handlePetNameOnChange.bind(this)
    this.handlePetRename = this.handlePetRename.bind(this)
    this.getNewSayings = this.getNewSayings.bind(this)
    this.initAPI = this.initAPI.bind(this)
  }

  async initAPI () {
    const [user, pet, ownedProducts] = await Promise.all([getCurrentUser(), getCurrentPet(), getOwnedProducts()])
    this.setState({
      pet,
      user,
      displayedPetName: pet.name,
      ownedProducts: ownedProducts.data,
    })
  }

  async componentDidMount () {
    await this.initAPI()
  }

  getNewSayings () {
    const sayingList = [
      '好好吃!!',
      '最喜歡吃罐罐>w<',
      '太好了^v^/',
      '...',
    ]
    const x = Math.floor(Math.random() * (sayingList.length))
    return sayingList[x]
  }

  handlePetNameOnChange (e) {
    this.setState({ displayedPetName: e.target.value })
  }

  handlePetRename (name) {
    const { pet } = this.state
    if (name === pet.name) { return }

    this.setState((prevState) => {
      const newPet = { ...prevState.pet }
      newPet.name = name
      return { pet: newPet, displayedPetName: name }
    }, async () => {
      await modifyCurrentPet({ name })
    })
  }

  handleModalButtonClick = () => {
    this.setState({
      shouldDisplayModal: false,
    })
  }

  handleErrorPetName =() => {
    this.setState({
      shouldDisplayModal: true,
      modalContent: 'Pet name cannot be empty!',
    })
  }

  handleUseOwnedProduct (id) {
    const { pet, ownedProducts } = this.state
    const { hunger } = pet
    if (hunger >= 12) {
      this.setState({
        shouldDisplayModal: true,
        modalContent: '吃飽了～',
      })
      return
    }

    const newOwnedProducts = [...ownedProducts]
    const usedOwnedProduct = newOwnedProducts.find(e => e._id === id)

    if (usedOwnedProduct.count > 0) { usedOwnedProduct.count -= 1 }

    this.setState((prevState) => {
      const newPet = { ...prevState.pet }
      newPet.hunger = Math.min(prevState.pet.hunger + usedOwnedProduct.product.energy, 12)
      return {
        pet: newPet,
        ownedProducts: newOwnedProducts,
        saying: this.getNewSayings(),
      }
    }, async () => {
      // TODO: owned product - 1, used owned product api should add hunger
      await useOwnedProduct({ _id: usedOwnedProduct._id })
    })
  }

  render () {
    const { isLogin } = this.context
    if (!isLogin) {
      if (isLogin === false) { Router.push('/login') }
      return (<Loading />)
    }

    const { user, pet, ownedProducts, saying, shouldDisplayModal, modalContent } = this.state
    const { money } = user

    const StoreIcon = (
      <Link href='/store'>
        <a>
          <Icon type='shoppingCart' textColor='text-secondary' textSize='text-4xl' />
        </a>
      </Link>
    )

    const modal = shouldDisplayModal
      ? (
        <div className='w-80 max-w-full fixed left-0 right-0 m-auto'>
          <div className='mx-4 rounded-lg shadow-lg bg-primary text-primary'>
            <Modal
              warningIcon={false}
              content={modalContent}
              rightButton='OK!'
              leftButton=''
              onRightButtonClick={this.handleModalButtonClick}
            />
          </div>
        </div>
        )
      : <></>
    return (
      <div>
        <HeaderBar
          leftUtils={StoreIcon}
          rightUtils={
            <div className='text-secondary flex items-center m-1'>
              <Icon type='money' textSize='text-2xl' />
              {money}
            </div>
          }
        />
        <Layout shouldFullScreen shouldPadHeaderBar shouldPadNavBar>
          {modal}
          <div className='flex flex-col justify-evenly bg-primary text-primary'>
            <PetName
              displayedPetName={this.state.displayedPetName}
              onChange={this.handlePetNameOnChange}
              onPetRename={this.handlePetRename}
              onHandleErrorPetName={this.handleErrorPetName}
            />
            <Pet pet={pet} saying={saying} />
            <PetStatus
              targetRead={user.target_read_article}
              alreadyRead={user.week_already_read}
              hunger={pet.hunger}
            />
            <OwnedProductList
              ownedProducts={ownedProducts}
              onItemClick={this.handleUseOwnedProduct}
            />
          </div>
        </Layout>
        <NavBar />
      </div>

    )
  }
}
PetPage.contextType = AuthContext
