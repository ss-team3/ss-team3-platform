import Router from 'next/router'

import Button from '../components/Button'
import Loading from '../components/Loading'

import { useAuth } from '../lib/utils'
import { signInWithGoogle } from '../lib/firebase'
import { signInSignUp } from '../api/user'

function Login () {
  const { isLogin } = useAuth({ shouldAuth: false, redirectTo: '/home' })
  if (typeof isLogin === 'undefined') return <Loading />

  const signIn = async () => {
    // Don't use the token return by signInWithGoogle()
    // get token from firebase.auth().currentUser.getIdToken(), instead.
    // ref: https://stackoverflow.com/questions/38335127/firebase-auth-id-token-has-incorrect-aud-claim
    const userCredential = await signInWithGoogle()
    const token = await userCredential.user.getIdToken(true)
    await signInSignUp({ token })
    Router.push('/home')
  }

  return (
    <div className='h-screen flex flex-col justify-center items-center gap-10'>
      <p className='font-bold font-serif text-4xl text-primary'>Product</p>
      <img src='/logo.png' />
      <Button onClick={signIn}>
        Login by Google
      </Button>
    </div>
  )
}

export default Login
