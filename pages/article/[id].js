/* eslint-disable camelcase */
import { Component } from 'react'
import Router from 'next/router'

import HeaderBar from '../../components/HeaderBar'
import Layout from '../../components/Layout'
import Icon from '../../components/Icon'
import ArticleSetting from '../../components/ArticleSetting'
import Loading from '../../components/Loading'
import Modal from '../../components/Modal'
import { AuthContext } from '../../components/providers/AuthProvider'

import {
  getCurrentArticle,
  modifyCurrentArticle,
  removeCurrentArticle,
} from '../../api/article'
import { getCurrentUser, modifyCurrentUser } from '../../api/user'

class Article extends Component {
  constructor (props) {
    super(props)
    this.state = {
      article: {},
      user: {},
      shouldDisplayModal: false,
      shouldDisplaySetting: false,
    }

    this.initAPI = this.initAPI.bind(this)

    this.handleClickSetting = this.handleClickSetting.bind(this)
    this.handleArchiveClick = this.handleArchiveClick.bind(this)
    this.handleDarkModeClick = this.handleDarkModeClick.bind(this)
    this.handleFontFamilyClick = this.handleFontFamilyClick.bind(this)
    this.handleFontSizeClick = this.handleFontSizeClick.bind(this)
    this.handleBookmarkClick = this.handleBookmarkClick.bind(this)
    this.handleDeleteClick = this.handleDeleteClick.bind(this)
  }

  async initAPI () {
    const { id: _id } = this.props
    const [article, user] = await Promise.all([getCurrentArticle({ _id }), getCurrentUser()])
    this.setTheme('light', user.theme)
    this.setState({ article, user })
  }

  async componentDidMount () {
    await this.initAPI()
  }

  setTheme (originTheme, newTheme) {
    const root = window.document.documentElement
    root.classList.remove(originTheme)
    root.classList.add(newTheme)
  }

  handleClickSetting () {
    this.setState((prevState) => { return { shouldDisplaySetting: !prevState.shouldDisplaySetting } })
  }

  async handleArchiveClick () {
    const { id: _id } = this.props
    const { article } = this.state
    const redirect = !article.archived
    await modifyCurrentArticle({ _id, archived: !article.archived })
    if (redirect) {
      Router.push({
        pathname: '/home',
        query: { articleId: _id },
      })
    } else {
      const updatedArticle = await getCurrentArticle({ _id })
      this.setState({ article: updatedArticle })
    }
  }

  handleDarkModeClick () {
    const { user } = this.state
    const newTheme = (user.theme === 'light' ? 'dark' : 'light')
    const newUserData = { ...user, theme: newTheme }
    this.setTheme(user.theme, newTheme)
    this.setState({ user: newUserData })
    modifyCurrentUser({ theme: newTheme })
  }

  handleFontFamilyClick (newFontFamily) {
    const { user } = this.state
    const newUserData = { ...user, font_family: newFontFamily }
    this.setState({ user: newUserData })
    modifyCurrentUser({ font_family: newFontFamily })
  }

  handleFontSizeClick (newFontSize) {
    if (newFontSize < 8 && newFontSize >= 0) {
      const { user } = this.state
      const newUserData = { ...user, font_size: newFontSize }
      this.setState({ user: newUserData })
      modifyCurrentUser({ font_size: newFontSize })
    }
  }

  handleBookmarkClick () {
    const { id: _id } = this.props
    const { article } = this.state
    const newArticle = { ...article, bookmark: !article.bookmark }
    this.setState({ article: newArticle })
    modifyCurrentArticle({ _id, bookmark: !article.bookmark })
  }

  handleDeleteClick () {
    const { id: _id } = this.props
    removeCurrentArticle({ _id })
    Router.back()
  }

  render () {
    const { isLogin } = this.context
    if (!isLogin) {
      if (isLogin === false) { Router.push('/login') }
      return (<Loading />)
    }

    const { user, article, shouldDisplayModal, shouldDisplaySetting } = this.state
    const { title, content, estimated_time, bookmark, archived_at } = article
    const { font_family = '', font_size = '' } = user
    const font_size_array = ['text-xs', 'text-sm', 'text-base', 'text-lg', 'text-xl', 'text-2xl', 'text-3xl', 'text-4xl']

    const buttonTextColor = (() => { return article.archived ? 'text-white' : 'text-secondary' })()
    const buttonBgColor = (() => { return article.archived ? 'bg-gray-300' : 'bg-primary' })()

    const audioIcon = (
      <Icon key='audioIcon' type='audio' textColor='text-secondary' textSize='text-2xl' />
    )
    const settingIcon = (
      <div key='settingIcon' className='cursor-pointer' onClick={this.handleClickSetting}>
        <Icon type='setting' textColor='text-secondary' textSize='text-2xl' />
      </div>
    )
    const headerBarUtils = [audioIcon, settingIcon]

    return (
      <>
        <HeaderBar title='' canNavBack rightUtils={headerBarUtils} />
        <Layout shouldPadHeaderBar shouldPadNavBar>
          <div className={`fixed top-14 right-2 ${shouldDisplaySetting ? 'block' : 'hidden'}`}>
            <ArticleSetting
              userData={user}
              bookmark={bookmark}
              onDarkModeClick={this.handleDarkModeClick}
              onFontFamilyClick={this.handleFontFamilyClick}
              onFontSizeClick={this.handleFontSizeClick}
              onBookmarkClick={this.handleBookmarkClick}
              onDeleteClick={() => this.setState({ shouldDisplayModal: true })}
            />
          </div>
          <div className={`w-max max-w-full fixed left-0 right-0 m-auto ${shouldDisplayModal ? 'block' : 'hidden'}`}>
            <div className='mx-4 rounded-lg shadow-lg bg-white text-primary'>
              <Modal
                onRightButtonClick={() => this.setState({ shouldDisplayModal: false })}
                onLeftButtonClick={this.handleDeleteClick}
                warningIcon
              />
            </div>
          </div>

          <div className='w-full md:w-auto mx-auto text-4xl text-center font-medium'>
            {title}
          </div>
          <div className='my-3 flex flex-col justify-center items-center gap-1 bg-primary'>
            <div className='text-center text-sm text-primary'>
              Reading time: {estimated_time} mins
            </div>
            <div className='text-center text-sm text-primary cursor-pointer'>
              <a href={article.url}>
                View original
              </a>
            </div>
          </div>

          <article className={`my-3 mx-auto prose text-primary break-words ${font_family} ${font_size_array[font_size]}`}>
            <div dangerouslySetInnerHTML={{ __html: content }} />
            {/* reference: https://stackoverflow.com/questions/19266197/reactjs-convert-html-string-to-jsx */}
          </article>

          <div
            className={`w-40 h-14 mx-auto mt-10 flex justify-center items-center shadow-lg gap-2 rounded-full border-2 border-text-secondary cursor-pointer ${buttonTextColor} ${buttonBgColor} text-xl `}
            onClick={this.handleArchiveClick}
          >
            <Icon type='archive' fill textColor={buttonTextColor} textSize='text-4xl' />
            <span className='my-auto text-center'>Archive</span>
          </div>
          {
          !archived_at &&
            <div className='w-full mx-auto mt-1 text-center text-base text-secondary'>
              $ + {estimated_time}
            </div>
        }
        </Layout>
      </>
    )
  }
}

Article.contextType = AuthContext

export default Article

export async function getStaticPaths () {
  /**
   * Since we deploy frontend and backend at the same time,
   * the following api call fails to reach the backend.
   * Thus we can't get the id when building
   */
  // const res = await getArticles()
  // const ids = res.data.map(article => article._id)
  // const paths = ids.map((id) => ({ params: { id } }))

  return {
    paths: [],
    fallback: true,
  }
}

export async function getStaticProps ({ params }) {
  // Fetch necessary data for the blog post using params.id
  const { id } = params
  return {
    props: {
      id: id,
    },
  }
}
// tutorial: https://nextjs.org/learn/basics/dynamic-routes/page-path-external-data
// doc: https://nextjs.org/docs/routing/dynamic-routes
