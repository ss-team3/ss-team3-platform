import { Component } from 'react'
import Router from 'next/router'

import HeaderBar from '../../components/HeaderBar'
import Icon from '../../components/Icon'
import Layout from '../../components/Layout'
import Loading from '../../components/Loading'
import Modal from '../../components/Modal'
import { AuthContext } from '../../components/providers/AuthProvider'

import { getCurrentCategories } from '../../api/category'
import { addCurrentArticle } from '../../api/article'

class AddArticle extends Component {
  constructor (props) {
    super(props)
    this.state = {
      shouldDisplayDropdownItem: false,
      shouldDisplayModal: false,
      modalContent: 'Add article successfully.',
      url: '',
      currentCategoryName: 'Category',
      currentCategoryId: '',
      bookmarked: false,
      categories: [],
    }

    this.handleDropdownClick = this.handleDropdownClick.bind(this)
    this.handleBookmarkClick = this.handleBookmarkClick.bind(this)
    this.handleUrlChange = this.handleUrlChange.bind(this)
    this.handleCategoryChange = this.handleCategoryChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  async initAPI () {
    const res = await getCurrentCategories()
    const initCategory = res.data.map(category => ({ name: category.name, _id: category._id }))
    this.setState({
      categories: initCategory,
      currentCategoryName: initCategory[0].name,
      currentCategoryId: initCategory[0]._id,
    })
  }

  async componentDidMount () {
    await this.initAPI()
  }

  handleDropdownClick () {
    this.setState({ shouldDisplayDropdownItem: !this.state.shouldDisplayDropdownItem })
  }

  handleBookmarkClick () {
    this.setState({ bookmarked: !this.state.bookmarked })
  }

  handleUrlChange (event) {
    this.setState({ url: event.target.value })
  }

  handleCategoryChange (_id, name) {
    this.setState({
      currentCategoryName: name,
      currentCategoryId: _id,
      shouldDisplayDropdownItem: !this.state.shouldDisplayDropdownItem,
    })
  }

  handleModalButtonClick = () => {
    this.setState({ shouldDisplayModal: false })
  }

  async handleSubmit () {
    const { url, currentCategoryId, bookmarked } = this.state

    if (!url) {
      this.setState({
        modalContent: 'Url can not be empty',
        shouldDisplayModal: true,
        currentCategoryName: this.state.categories[0].name,
        currentCategoryId: this.state.categories[0]._id,
        url: '',
      })
      return
    }

    if (!url || !currentCategoryId) {
      throw new Error('To add article, `url`, `category` cannot be empty')
    }

    try {
      await addCurrentArticle({
        category_id: currentCategoryId,
        url,
        bookmark: bookmarked,
      })
      this.setState({
        modalContent: 'Add article successfully.',
        shouldDisplayModal: true,
        currentCategoryName: this.state.categories[0].name,
        currentCategoryId: this.state.categories[0]._id,
        url: '',
      })
    } catch (err) {
      this.setState({
        modalContent: 'Fail to add article.',
        shouldDisplayModal: true,
        currentCategoryName: this.state.categories[0].name,
        currentCategoryId: this.state.categories[0]._id,
        url: '',
      })
    }
  }

  render () {
    const { isLogin } = this.context
    if (!isLogin) {
      if (isLogin === false) { Router.push('/login') }
      return (<Loading />)
    }

    const { state } = this
    const { shouldDisplayDropdownItem, shouldDisplayModal, modalContent, bookmarked, categories, currentCategoryName, url } = state
    const categoryList = []
    for (const datum of categories) {
      categoryList.push(
        <li
          key={datum._id}
          name={datum.name}
          value={datum._id}
          className='bg-secondary hover:bg-gray-200 py-2 px-4 block'
          onClick={() => { this.handleCategoryChange(datum._id, datum.name) }}
        >
          {datum.name}
        </li>,
      )
    }
    return (
      <>
        <HeaderBar title='Add Article' canNavBack />
        <Layout shouldPadHeaderBar shouldPadNavBar>
          <div className={`w-80 max-w-full fixed left-0 right-0 m-auto ${shouldDisplayModal ? 'block' : 'hidden'}`}>
            <div className='mx-4 rounded-lg shadow-lg bg-primary text-primary'>
              <Modal
                warningIcon={false}
                content={modalContent}
                rightButton='OK!'
                leftButton=''
                onRightButtonClick={this.handleModalButtonClick}
              />
            </div>
          </div>
          <div className='flex flex-col gap-1 w-80 justify-center mx-auto mt-8'>
            <span className='text-xl'>Url</span>
            <div className='flex w-full items-center border-b py-1 mb-5 text-lg'>
              <input
                className='w-full bg-primary text-primary py-1 pl-3 focus:outline-none'
                placeholder='https://article.com/post'
                value={url}
                onChange={this.handleUrlChange}
              />
            </div>
            <span className='text-xl'>Category</span>
            <div>
              <button
                className='w-full h-11 pl-3 text-primary border-b focus:outline-none inline-flex items-center justify-end'
                onClick={() => this.handleDropdownClick()}
              >
                <span className='mr-auto text-lg'>{currentCategoryName}</span>
                <Icon type='dropdown' textColor='text-secondary' className={!shouldDisplayDropdownItem || 'transform rotate-180'} />
              </button>
              <ul className={`${shouldDisplayDropdownItem ? 'block' : 'hidden'} text-primary text-lg pt-1`}>
                {categoryList}
              </ul>
            </div>
            <div className='flex cursor-pointer items-center mt-5 gap-1.5 text-xl' onClick={this.handleBookmarkClick}>
              <Icon type='bookmark' fill={bookmarked} textColor='text-secondary' />
              Bookmark
            </div>
            <div
              className='w-36 h-14 cursor-pointer flex gap-1 rounded-full border-2 border-secondary text-2xl justify-center items-center mx-auto mt-24 shadow-xl hover:shadow'
              onClick={this.handleSubmit}
            >
              <Icon type='add' textColor='text-primary' />
              <span className='text-primary'>Add</span>
            </div>
          </div>
        </Layout>
      </>
    )
  }
}

AddArticle.contextType = AuthContext

export default AddArticle
