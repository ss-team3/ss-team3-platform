import Link from 'next/link'
import { Component } from 'react'
import Avatar from './Avatar'

class CategoryListItem extends Component {
  render () {
    const { props } = this
    const { data, specialHref = null, specialAvatar = null } = props
    const { _id, name, avatar_url: avatarUrl, articles } = data
    const totalArticles = articles.length
    const href = (specialHref || _id)
    // FIXME: no post const property in schema

    return (
      <Link href={`/category/${href}`}>
        <a className='w-full p-4 rounded-lg shadow-md hover:shadow-lg flex items-center gap-3'>
          {specialAvatar ||
            <Avatar
              type={avatarUrl ? 'image' : 'text'}
              size='w-16 h-16'
              className='bg-secondary'
              text={name}
              url={avatarUrl}
            />}
          <div className='flex flex-col'>
            <span className='font-medium'>{name}</span>
            <span className='text-secondary'>{totalArticles} posts</span>
          </div>
        </a>
      </Link>
    )
  }
}

export default CategoryListItem
