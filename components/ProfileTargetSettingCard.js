import React, { Component } from 'react'

/**
 * @param {Object} props.type - category list
 */
export default class ProfileTargetSettingCard extends Component {
  constructor (props) {
    super(props)
    this.state = {
      pendingTarget: 1,
    }
    this.handleOkClick = this.handleOkClick.bind(this)
    this.inputElement = React.createRef()
  }

  handleOkClick (e) {
    const { pendingTarget } = this.state
    const { onChangeTarget } = this.props
    onChangeTarget(pendingTarget)
    this.setState({ pendingTarget })
  }

  filterInput (value) {
    const onlyNumberArr = [...value].filter(c => Number(c) === 0 ? true : Number(c))
    return Number(onlyNumberArr.join(''))
  }

  render () {
    const { filterInput } = this
    const { targetRead } = this.props
    const { pendingTarget } = this.state

    return (
      <div className='w-full flex flex-row items-center justify-between my-2'>
        <div className='items-center text-primary'>
          <span className='text-xl text-primary'>Target Read</span>
          <span className='text-sm text-secondary m-1'>/week</span>
        </div>
        <div className={`flex items-center text-primary bg-primary rounded-lg shadow-md ${(pendingTarget && (isNaN(pendingTarget) || Number(pendingTarget)) <= 0) && 'ring-2 ring-red-400'} `}>
          <input
            placeholder={targetRead}
            value={pendingTarget}
            onChange={(e) => { this.setState({ pendingTarget: filterInput(e.target.value) }) }}
            className='w-24 text-primary bg-primary rounded-l-lg py-1 px-2 leading-tight focus:outline-none appearance-none'
            ref={this.inputElement}
          />
          <button
            className='flex-shrink-0 bg-gray-300 text-sm font-semibold text-white py-1 px-2 rounded-r-lg focus:outline-none'
            type='button'
            onClick={() => this.handleOkClick()}
          >
            OK
          </button>
        </div>

      </div>
    )
  }
}
