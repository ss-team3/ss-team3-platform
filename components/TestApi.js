import { Component } from 'react'
import Button from './Button'
import { getCurrentUser } from '../api/user'
import { AuthContext } from './providers/AuthProvider'

class TestApi extends Component {
  constructor (props) {
    super(props)
    this.state = {
      res: '',
    }

    this.refreshCurrentUser = this.refreshCurrentUser.bind(this)
  }

  async componentDidMount () {
    await this.refreshCurrentUser()
  }

  async refreshCurrentUser () {
    try {
      const res = await getCurrentUser()
      this.setState({ res: res })
    } catch (err) {
      this.setState({ res: null })
      throw err
    }
  }

  render () {
    const { res } = this.state

    return (
      <div className='flex flex-col justify-between items-center'>

        <Button
          textColor='text-white'
          bgColor='bg-secondary'
          onClick={() => { this.refreshCurrentUser() }}
        >
          refresh current user
        </Button>

        <div className='p-2 flex flex-col items-center'>
          <p className='text-primary text-lg'>Current User</p>
          <p className='w-2/3 h-1/3'>{JSON.stringify(res, null, 2)}</p>
        </div>
        <div className='p-2 flex flex-col items-center'>
          <p className='text-primary text-lg'>Current firebase user</p>
          <AuthContext.Consumer>
            {value => <p className='w-2/3 h-1/3'>{JSON.stringify(value, null, 2)}</p>}
          </AuthContext.Consumer>
        </div>

      </div>
    )
  }
}

export default TestApi
