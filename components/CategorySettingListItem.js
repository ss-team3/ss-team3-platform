import React, { Component } from 'react'
import Link from 'next/link'
import Avatar from './Avatar'
import Icon from './Icon'

class CategorySettingListItem extends Component {
  constructor (props) {
    super(props)
    this.state = {
      isEditing: false,
      pendingName: '',
    }
    this.inputElement = React.createRef()
    this.handleRenameClick = this.handleRenameClick.bind(this)
  }

  componentDidMount () {
    this.setState({ pendingName: this.props.data.name })
  }

  handleRenameClick (id) {
    const { isEditing, pendingName } = this.state
    const { onCategoryRename } = this.props

    if (!isEditing) {
      this.inputElement.current.focus()
      this.inputElement.current.select()
      this.setState({ isEditing: true })
    } else {
      // situation: user submit new name
      const success = onCategoryRename(id, pendingName)
      if (success) {
        this.setState({ isEditing: false })
      } else {
        this.setState({ pendingName: this.props.data.name, isEditing: false })
      }
    }
  }

  handleRemoveClick (id) {
    this.props.onCategoryRemove(id)
  }

  render () {
    const { data, totalArticles = 0 } = this.props
    //  FIXME: why set `_id` default to "000" ? -> ask YenTina
    const { _id, name, avatar_url: avatarUrl } = data
    const { isEditing, pendingName } = this.state

    /*
    let href = ''
    if (isEditing) {
      href = '/profile/categoriesSetting'
    } else {
      href = `/category/${name === 'All' ? 'All' : _id}`
    }
    */

    const image = !isEditing
      ? (
        <Avatar
          type={avatarUrl ? 'image' : 'text'}
          size='w-16 h-16'
          className='bg-secondary flex-shrink-0'
          text={name}
          url=''
        />
        )
      : (
        <Avatar
          type={avatarUrl ? 'image' : 'text'}
          size='w-16 h-16'
          className='bg-secondary'
          text={pendingName || ' '}
          url=''
        />
        )

    const editButton = (
      isEditing
        ? <Icon type='done' fill textColor='text-secondary hover:text-primary' textSize='text-3xl' />
        : <Icon type='edit' fill textColor='text-secondary hover:text-primary' textSize='text-3xl' />
    )
    return (
      <div className='flex flex-row items-center w-full'>
        <button onClick={() => this.handleRemoveClick(_id)} className='mr-3 '>
          <Icon fill type='remove' textSize='text-3xl' textColor='text-red-400 hover:text-red-600' />
        </button>
        {/* <Link href={href}> */}
        {/* <a className='w-full p-4 rounded-lg shadow-md hover:shadow-lg flex items-center gap-3'> */}
        <div className='w-full p-4 rounded-lg shadow-md flex items-center gap-3'>
          {image}
          <div className='flex flex-col w-2/3 text-gray-500 bg-primary'>
            <input
              type='text'
              placeholder={name}
              value={isEditing ? pendingName : name}
              readOnly={!isEditing}
              onChange={(e) => { this.setState({ pendingName: e.target.value }) }}
              ref={this.inputElement}
              className={`font-medium bg-primary px-1 rounded-md text-primary w-full ${isEditing ? 'border-2' : ''}`}
            />
            <span className='text-secondary px-1'>{totalArticles} posts</span>
          </div>
        </div>
        {/* </a> */}
        {/* </Link> */}
        <button onClick={() => this.handleRenameClick(_id)} className='ml-3'>
          {editButton}
        </button>
      </div>
    )
  }
}

export default CategorySettingListItem
