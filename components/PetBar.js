import { Component } from 'react'
import Link from 'next/link'
import Avatar from '../components/Avatar'
import Icon from '../components/Icon'

/**
 * @param {Number} props.targetRead - target reading amount
 * @param {Number} props.alreadyRead - already read amount
 * @param {Number} props.hunger - Display the hungry degree. Default full hunger is 12.
 */
class PetBar extends Component {
  render () {
    const { props } = this
    const { targetRead = 0, alreadyRead = 0, pet = {} } = props
    const { hunger = 0, type = 'cat' } = pet
    const readPercent = alreadyRead / targetRead
    const readPercentStyles = { width: `${readPercent * 100}%` }
    const unreadPercentStyles = { width: readPercent > 1 ? '0%' : `${(1 - readPercent) * 100}%` }
    const hungerPercent = hunger / 12
    const hungerPercentStyles = { width: `${hungerPercent * 100}%` }
    const unhungerPercentStyles = { width: `${(1 - hungerPercent) * 100}%` }
    const url = `/pet/${type}.png`

    return (
      <Link href='/pet'>
        <a className='flex flex-row justify-around items-center gap-2 md:gap-6 shadow-md rounded-lg p-3'>
          <Avatar
            type='image'
            size='w-20 h-20 md:w-28 md:h-28'
            url={url}
          />
          <div className='w-3/4 flex flex-col justify-evenly items-center'>
            <div className='w-full flex flex-row justify-center items-center gap-2'>
              <Icon type='heart' fill textColor='text-pink-200' textSize='text-2xl' />
              <div className='w-full h-2 border-0 border-secondary my-auto shadow-md flex flex-row'>
                <div className='h-full bg-pink-200' style={readPercentStyles} />
                <div className='h-full bg-gray-100' style={unreadPercentStyles} />
              </div>
            </div>
            <div className='w-full flex flex-row justify-center items-center gap-2'>
              <Icon type='tableware' textColor='text-blue-300' textSize='text-2xl' />
              <div className='w-full h-2 border-0 border-secondary my-auto shadow-md flex flex-row'>
                <div className='h-full bg-blue-300' style={hungerPercentStyles} />
                <div className='h-full bg-gray-100' style={unhungerPercentStyles} />
              </div>
            </div>
          </div>
        </a>
      </Link>
    )
  }
}

export default PetBar
