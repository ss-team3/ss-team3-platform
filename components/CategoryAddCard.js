import React, { Component } from 'react'
import Avatar from './Avatar'
import Icon from './Icon'

class CategoryAddCard extends Component {
  constructor (props) {
    super(props)
    this.state = {
      pendingName: '',
    }
    this.inputElement = React.createRef()
    this.handleAddClick = this.handleAddClick.bind(this)
  }

  handleAddClick (id) {
    const { pendingName } = this.state
    const { onCategoryAdd } = this.props
    onCategoryAdd(pendingName)
    this.setState({ pendingName: '' })
  }

  render () {
    const { pendingName } = this.state
    const avatar = pendingName
      ? (
        <Avatar
          type='text'
          size='w-16 h-16'
          className='bg-secondary'
          text={pendingName}
          url=''
        />
        )
      : <div className='w-16 h-16 bg-secondary rounded-full' />
    return (
      <div className='w-full p-4 rounded-2xl shadow-md flex justify-center items-center gap-3 border border-secondary'>
        <div>
          {avatar}
        </div>
        <div className='w-full'>
          <input
            type='text'
            placeholder='new category name'
            value={pendingName}
            onChange={(e) => { this.setState(() => { return { pendingName: e.target.value } }) }}
            ref={this.inputElement}
            className='font-medium h-11 border-2 focus:border-gray-500 rounded-lg p-2 bg-primary text-primary w-full'
          />
        </div>
        <button
          onClick={() => this.handleAddClick()}
          className='bg-secondary hover:bg-gray-500 rounded-full px-2 py-2 flex flex-row items-center justify-center'
        >
          <p className='flex flex-row items-center'>
            <Icon type='add' textColor='text-white' textSize='text-xl' />
            <span className='text-white text-md font-semibold'>ADD</span>
          </p>
        </button>
      </div>
    )
  }
}

export default CategoryAddCard
