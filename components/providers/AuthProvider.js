import { Component, createContext } from 'react'
import { auth } from '../../lib/firebase'

export const AuthContext = createContext(undefined)

class AuthProvider extends Component {
  constructor (props) {
    super(props)

    this.state = {
      isLogin: undefined,
      userAuth: {},
    }
  }

  componentDidMount () {
    auth.onAuthStateChanged(async userAuth => {
      let newUserAuth = {}
      if (userAuth) {
        const { name, email, photoURL: photoUrl, uid } = userAuth
        newUserAuth = { name, email, photoUrl, uid }
      }

      // setup api headers
      if (userAuth) {
        const token = await auth.currentUser.getIdToken(true)
        window.localStorage.setItem('auth_token', token)
        // console.log('[debug - token]', token) // TODO: for debug
      } else {
        window.localStorage.removeItem('auth_token')
        // console.log('[debug - token] token removed') // TODO: for debug
      }

      this.setState({
        isLogin: (!!userAuth),
        userAuth: newUserAuth,
      })
    })
  }

  render () {
    return (
      <AuthContext.Provider value={this.state}>
        {this.props.children}
      </AuthContext.Provider>
    )
  }
}

export default AuthProvider
