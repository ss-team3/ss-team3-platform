import { Component } from 'react'
import Icon from './Icon'

class NavBackButton extends Component {
  handleNavBack () {
    // TODO: if history.back out of our site, redirect to /home
    window.history.back()
  };

  render () {
    return (
      <button
        onClick={this.handleNavBack}
        className='text-primary bg-background inline-flex items-center'
      >
        <Icon type='navBack' textColor='text-secondary' textSize='text-4xl' />
      </button>
    )
  }
}

// For hover:
// hover:border-transparent hover:text-background hover:bg-primary
export default NavBackButton
