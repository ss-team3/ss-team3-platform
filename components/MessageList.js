import { Component } from 'react'
import _ from 'lodash'
import Message from '../components/Message'

class MessageList extends Component {
  render () {
    const { props } = this
    const { pet, messages, onArticleClick } = props
    const sortedMessages = _.orderBy(messages, ['sent_at'], ['desc'])

    const messageList = []
    for (const datum of sortedMessages) {
      messageList.push(
        <Message
          key={datum._id}
          message={datum}
          pet={pet}
          onArticleClick={onArticleClick}
        />,
      )
    }

    return (
      <>
        <div className='flex flex-col-reverse'>{messageList}</div>
      </>
    )
  }
}

export default MessageList
