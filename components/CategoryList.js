import { Component } from 'react'
import CategoryListItem from './CategoryListItem'

class CategoryList extends Component {
  render () {
    const { props } = this
    const { categories } = props
    const { data } = categories
    const categoryComponents = []

    for (const datum of data) {
      const { _id, name } = datum
      if (name === 'All') continue
      categoryComponents.push(
        <CategoryListItem
          key={_id}
          data={datum}
        />,
      )
    }
    return (
      <div className='w-full flex flex-col gap-4 items-center'>
        {categoryComponents}
      </div>
    )
  }
}

export default CategoryList
