/* eslint-disable camelcase */
import { Component } from 'react'
import Avatar from './Avatar'
import Icon from './Icon'
import { AuthContext } from './providers/AuthProvider'

class Message extends Component {
  render () {
    const { props } = this
    const { photoUrl } = this.context.userAuth
    const { pet, message, onArticleClick } = props
    const { article, content, type, send_to_pet, article_id } = message
    const flexDirection = !send_to_pet ? 'flex-row mr-auto' : 'flex-row-reverse ml-auto'
    const avatarUrl = send_to_pet ? photoUrl : `/pet/${pet.type}.png`

    let MessageBlock
    if (type === 'message') {
      MessageBlock = (
        <div className='w-auto h-auto px-3 py-2 rounded-3xl border border-secondary'>
          {content}
        </div>
      )
    } else {
      MessageBlock = (
        <div className='cursor-pointer w-1/2' onClick={() => onArticleClick(article_id)}>
          <div className='w-full h-auto border border-secondary border-b-0 rounded-t-3xl'>
            <img className='w-full  rounded-t-3xl object-cover' src={article.cover_url} />
          </div>
          <div className='flex flex-row items-center justify-between w-full h-auto px-3 py-1 text-primary border border-secondary rounded-b-3xl'>
            <div className='ml-2'>{article.estimated_time} mins to read</div>
            <Icon type='readMore' textColor='text-secondary' textSize='text-2xl' />
          </div>
        </div>
      )
    }

    return (
      <div className={`flex ${flexDirection} gap-3 my-3`}>
        <Avatar type='image' url={avatarUrl} size='w-10 h-10' className='flex-none' />
        {MessageBlock}
      </div>
    )
  }
}

Message.contextType = AuthContext

export default Message
