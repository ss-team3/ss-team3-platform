import { Component } from 'react'

/**
 * @param {String} props.saying
 * @param {String} props.catPicture
 */
class Pet extends Component {
  render () {
    const { saying, pet } = this.props
    const { type = 'default_pet' } = pet
    return (
      <div className='flex flex-col justify-center items-center gap-2'>
        <p className='w-1/3 p-4 shadow-md rounded-lg flex justify-center'>{saying}</p>
        <img src={`/pet/${type}.png`} alt='pet' />
      </div>
    )
  }
}

export default Pet
