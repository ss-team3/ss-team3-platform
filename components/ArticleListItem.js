import { Component } from 'react'
import Link from 'next/link'
import Icon from '../components/Icon'

class ArticleListItem extends Component {
  render () {
    const { props } = this
    const { article, onChangeBookmarkArchived } = props
    const { _id, title, cover_url: coverUrl, estimated_time: estimatedTime, bookmark: isBookmarked, archived: isArchived } = article

    return (
      <Link href={`/article/${_id}`}>
        <a className='w-full flex flex-row justify-between items-center gap-3 rounded-lg shadow-md'>
          <div className='w-2/3 pl-4 py-2 flex flex-col gap-2'>
            <div className='font-medium text-xl'>
              {title}
            </div>
            <div className='flex flex-row justify-between items-center gap-2'>
              <div className='w-max flex flex-row items-center gap-2'>
                <Icon type='time' textColor='text-secondary' textSize='text-xl' />
                <span className='text-secondary'>{estimatedTime} mins</span>
              </div>
              <div className='w-max flex flex-row gap-2'>
                <div onClick={(e) => { e.preventDefault(); onChangeBookmarkArchived(_id, !isBookmarked, null) }}>
                  <Icon type='bookmark' fill={isBookmarked} textColor='text-secondary' textSize='text-xl' />
                </div>
                <div onClick={(e) => { e.preventDefault(); onChangeBookmarkArchived(_id, null, !isArchived) }}>
                  <Icon type='archive' fill={isArchived} textColor='text-secondary' textSize='text-xl' />
                </div>
              </div>
            </div>
          </div>
          <div className='w-1/3 relative'>
            <img className='rounded-r-lg object-cover' src={coverUrl} alt={coverUrl} />
            <div hidden={!isBookmarked} className='absolute -top-2 right-2'>
              <Icon type='bookmark' fill textColor='text-bookmark' textSize='text-xl' />
            </div>
          </div>
        </a>
      </Link>
    )
  }
}

export default ArticleListItem
