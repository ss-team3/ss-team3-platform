import React, { Component } from 'react'
import ScrollMenu from 'react-horizontal-scrolling-menu'
import Icon from './Icon'

// Arrow component
const MenuArrow = ({ type, className }) => {
  return (
    <div className={className}>
      <Icon type={type} textColor='text-secondary' textSize='text-4xl' />
    </div>
  )
}
const ArrowLeft = MenuArrow({ type: 'navBack', className: 'arrow-prev' })
const ArrowRight = MenuArrow({ type: 'navNext', className: 'arrow-next' })

class OwnedProductList extends Component {
  constructor (props) {
    super(props)
    this.state = {
      selectedKey: '',
    }
  }

  handleOnClick = key => {
    this.setState({ selected: key })
    this.props.onItemClick(key)
  }

  render () {
    const { selectedKey } = this.state
    const { ownedProducts } = this.props

    const menuItems = ownedProducts.filter(i => i.count > 0).map(ownedProduct => {
      const { _id, count, product = {} } = ownedProduct
      const { name } = product

      return (
        <div
          key={_id}
          className={`group relative ${(_id === selectedKey) ? 'active' : ''}`}
        >
          <img
            src={`/product/${name}.png`}
            alt={name}
            className='w-20 h-20 m-2 transition duration-300 ease-in-out transform group-hover:scale-110'
          />
          <button className='absolute top-0 right-0 w-5 h-5 text-center text-sm text-white rounded-full bg-red-400 group-hover:scale-110 transition duration-300 ease-in-out transform '>
            {count}
          </button>
        </div>
      )
    })

    return (
      <div className='flex p-4 justify-center m-4'>
        {menuItems.length
          ? <ScrollMenu
              data={menuItems}
              arrowLeft={ArrowLeft}
              arrowRight={ArrowRight}
              selected={selectedKey}
              onSelect={this.handleOnClick}
            />
          : <div className='secondary text-gray-400'>You do not owned any product. Buy some in Store</div>}
      </div>
    )
  }
}

export default OwnedProductList
