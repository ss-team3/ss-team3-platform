import { Component } from 'react'
import Icon from '../components/Icon'

/**
 * @param {Number} props.targetRead - target reading amount
 * @param {Number} props.alreadyRead - already read amount
 * @param {Number} props.hunger - Display the hungry degree. Default full hunger is 12.
 */
class PetStatus extends Component {
  getCatFeeling (hunger) {
    if (hunger < 12) {
      return 'still hungry'
    } else {
      return 'full'
    }
  }

  render () {
    const { props } = this
    const { targetRead = 0, alreadyRead = 0, hunger } = props
    const readPercent = alreadyRead / targetRead
    const readPercentStyles = { width: `${readPercent * 100}%` }
    const unreadPercentStyles = { width: `${(1 - readPercent) * 100}%` }
    const hungerPercent = hunger / 12
    const hungerPercentStyles = { width: `${hungerPercent * 100}%` }
    const unhungerPercentStyles = { width: `${(1 - hungerPercent) * 100}%` }

    return (
      <div className='flex flex-row justify-center items-center gap-4'>
        <div className='flex flex-col justify-evenly items-center'>
          <div className='flex flex-row justify-center items-center gap-2'>
            <Icon type='heart' fill textColor='text-pink-200' textSize='text-2xl' />
            <div className='w-48 h-2 md:w-60 border-0 border-secondary my-auto shadow-md flex flex-row'>
              <div className='h-full bg-pink-200' style={readPercentStyles} />
              <div className='h-full bg-gray-100' style={unreadPercentStyles} />
            </div>
          </div>
          <p>{alreadyRead}/{targetRead} posts a week.</p>
          <div className='flex flex-row justify-center items-center gap-2'>
            <Icon type='tableware' textColor='text-blue-300' textSize='text-2xl' />
            <div className='w-48 h-2 md:w-60 border-0 border-secondary my-auto shadow-md flex flex-row'>
              <div className='h-full bg-blue-300' style={hungerPercentStyles} />
              <div className='h-full bg-gray-100' style={unhungerPercentStyles} />
            </div>
          </div>
          <p>{`The Cat is ${this.getCatFeeling(hunger)} now.`}</p>
        </div>
      </div>
    )
  }
}

export default PetStatus
