import { Component } from 'react'
import HeaderBar from './HeaderBar'
import Avatar from './Avatar'

/**
 * @param {String} props.pet - the pet object (with name and type)
 */

class PetHeaderBar extends Component {
  render () {
    const { pet = {} } = this.props
    const { name = '貓貓', type = 'cat' } = pet
    const url = `/pet/${type}.png`
    const petAvatar = (
      <div className='flex flex-row justify-start items-center gap-4'>
        <Avatar type='image' size='h-11 w-11' url={url} className='border border-secondary' />
        <p className='text-2xl text-primary'>{name}</p>
      </div>
    )
    return (
      <div>
        <HeaderBar title='' canNavBack leftUtils={petAvatar} />
      </div>
    )
  }
}
export default PetHeaderBar
