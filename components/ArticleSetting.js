/* eslint-disable camelcase */
import { Component } from 'react'
import Icon from '../components/Icon'

class ArticleSetting extends Component {
  render () {
    const { props } = this
    const { userData, bookmark, onDarkModeClick, onFontFamilyClick, onFontSizeClick, onBookmarkClick, onDeleteClick } = props
    const { theme, font_family, font_size } = userData

    const sizeBar = []
    for (let i = 0; i < 8; i++) {
      sizeBar.push(
        (i <= font_size)
          ? <p key={i} className='font-serif text-2xl text-blue-300'>·</p>
          : <p key={i} className='font-serif text-2xl text-secondary'>·</p>,
      )
    }

    return (
      <div className='flex flex-col gap-1 select-none w-max p-4 rounded-lg shadow-lg border-secondary bg-primary border-2 border-primary'>
        {/* light/dark mode */}
        <div
          className='flex flex-row gap-5 justify-center cursor-pointer'
          onClick={onDarkModeClick}
        >
          <Icon type='settingNightMode' fill={theme !== 'light'} textColor='text-secondary' textSize='text-3xl' />
        </div>

        {/* font style */}
        <div className='flex flex-row gap-6 justify-center'>
          <p className={`font-sans text-2xl cursor-pointer ${font_family === 'font-sans' ? 'text-blue-300' : 'text-secondary'}`} onClick={() => onFontFamilyClick('font-sans')}>Aa</p>
          <p className={`font-serif text-2xl cursor-pointer ${font_family === 'font-serif' ? 'text-blue-300' : 'text-secondary'}`} onClick={() => onFontFamilyClick('font-serif')}>Aa</p>
          <p className={`font-mono text-2xl cursor-pointer ${font_family === 'font-mono' ? 'text-blue-300' : 'text-secondary'}`} onClick={() => onFontFamilyClick('font-mono')}>Aa</p>
        </div>

        {/* font size */}
        <div className='flex flex-row justify-center'>
          <p className='cursor-pointer font-sans text-2xl text-secondary mr-3' onClick={() => onFontSizeClick(font_size - 1)}>A-</p>
          {sizeBar}
          <p className='cursor-pointer font-sans text-2xl text-secondary ml-3' onClick={() => onFontSizeClick(font_size + 1)}>A+</p>
        </div>

        {/* email this article to me */}
        {/* <div className='flex flex-row gap-4 justify-start text-secondary ml-2'>
          <Icon type='email' textColor='text-secondary' fill textSize='text-3xl' />
          <span className='my-auto text-center '> Email to me </span>
        </div> */}

        <div className='flex flex-row gap-4 justify-start text-secondary cursor-pointer' onClick={onBookmarkClick}>
          <Icon type='bookmark' textColor='text-secondary' textSize='text-3xl' fill={bookmark === true} />
          <span className='my-auto text-center'> Bookmark </span>
        </div>

        <div className='flex flex-row gap-4 justify-start text-secondary cursor-pointer' onClick={onDeleteClick}>
          <Icon type='delete' textColor='text-secondary' fill textSize='text-3xl' />
          <span className='my-auto text-center'> Delete </span>
        </div>
      </div>
    )
  }
}

export default ArticleSetting
