import { Component } from 'react'
import Icon from './Icon'
import CategoryListItem from './CategoryListItem'

class DefaultCategoryList extends Component {
  render () {
    const { props } = this
    const { posts } = props
    const { data } = posts
    // FIXME: no post const property in schema

    return (
      <>
        <CategoryListItem
          specialHref='bookmark'
          specialAvatar={
            <div className='w-16 h-16 bg-background rounded-full items-center'>
              <Icon type='bookmark' textColor='text-bookmark' textSize='text-6xl' style={{ fontSize: '3.75rem' }} />
            </div>
          }
          data={{
            name: 'bookmark',
            articles: data.filter(i => i.bookmark),
          }}
        />
        <CategoryListItem
          specialHref='All'
          specialAvatar={
            <div className='w-16 h-16 bg-secondary rounded-full flex flex-col justify-center items-center'>
              <p className='text-white text-2xl'>All</p>
            </div>
          }
          data={{
            name: 'All articles',
            articles: data,
          }}
        />
      </>
    )
  }
}

export default DefaultCategoryList
