import { Component } from 'react'
import Button from './Button'
import Icon from './Icon'

class ChatMessageInput extends Component {
  constructor (props) {
    super(props)
    this.state = {
      isChoosing: false,
    }
    this.handleQuickClick = this.handleQuickClick.bind(this)
    this.handleShortClick = this.handleShortClick.bind(this)
    this.handleLongClick = this.handleLongClick.bind(this)
    this.handleFocusClick = this.handleFocusClick.bind(this)
    this.handleAskClick = this.handleAskClick.bind(this)
  }

  handleQuickClick () {
    const { onClick } = this.props
    this.setState({ isChoosing: false })
    onClick('quick')
  }

  handleShortClick () {
    const { onClick } = this.props
    this.setState({ isChoosing: false })
    onClick('short')
  }

  handleLongClick () {
    const { onClick } = this.props
    this.setState({ isChoosing: false })
    onClick('long')
  }

  handleFocusClick () {
    const { onClick } = this.props
    this.setState({ isChoosing: false })
    onClick('focus')
  }

  handleAskClick () {
    const { onClick } = this.props
    this.setState({ isChoosing: true })
    onClick('recommend')
  }

  render () {
    const { isChoosing } = this.state

    const ask = (
      <div className='fixed bottom-0 w-full h-16 flex flex-row justify-around items-center bg-secondary'>
        <Button
          textColor='text-primary'
          bgColor='bg-secondary hover:bg-primary'
          className='w-full h-11 flex flex-row items-center sm:w-1/3 justify-center hover:shadow-inner'
          onClick={this.handleAskClick}
        >
          <Icon type='smile' textColor='text-primary' textSize='text-3xl' />
          &nbsp;推薦一篇文章
        </Button>
      </div>
    )

    const choice = (
      <div className='fixed bottom-0 w-full h-16 flex flex-row justify-around items-center bg-secondary'>
        <Button
          textColor='text-primary'
          bgColor='bg-secondary hover:bg-primary'
          className='w-1/5 h-11 hover:shadow-inner'
          onClick={this.handleQuickClick}
        >
          Quick
        </Button>
        <Button
          textColor='text-primary'
          bgColor='bg-secondary hover:bg-primary'
          className='w-1/5 h-11 hover:shadow-inner'
          onClick={this.handleShortClick}
        >
          Short
        </Button>
        <Button
          textColor='text-primary'
          bgColor='bg-secondary hover:bg-primary'
          className='w-1/5 h-11 hover:shadow-inner'
          onClick={this.handleLongClick}
        >
          Long
        </Button>
        <Button
          textColor='text-primary'
          bgColor='bg-secondary hover:bg-primary'
          className='w-1/5 h-11 hover:shadow-inner'
          onClick={this.handleFocusClick}
        >
          Focus
        </Button>
      </div>
    )

    return (
      <>
        <div className={isChoosing ? '' : 'hidden'}>
          {choice}
        </div>
        <div className={isChoosing ? 'hidden' : ''}>
          {ask}
        </div>
      </>
    )
  }
}
export default ChatMessageInput
