import { Component } from 'react'
import Icon from '../components/Icon'

class PetName extends Component {
  constructor (props) {
    super(props)
    this.state = {
      canChange: false,
    }

    this.handleRenameClick = this.handleRenameClick.bind(this)

    // input element Ref
    // ref: React - Callback Refs: https://zh-hant.reactjs.org/docs/refs-and-the-dom.html
    this.inputElement = null
    this.setInputElementRef = element => {
      this.inputElement = element
    }
  }

  handleRenameClick (e) {
    const { canChange } = this.state
    const { displayedPetName, onPetRename, onHandleErrorPetName } = this.props

    if (!canChange) {
      this.inputElement.focus()
      this.inputElement.select()
      this.setState({ canChange: true })
    } else {
      // situation: user submit new name
      if (!displayedPetName) {
        onHandleErrorPetName()
        return
      }
      onPetRename(displayedPetName)
      this.setState({ canChange: false })
    }
  }

  render () {
    const { canChange } = this.state
    const { displayedPetName, onChange } = this.props

    const editButton = (
      canChange
        ? <Icon type='done' fill textColor='text-secondary' textSize='text-2xl' />
        : <Icon type='edit' fill textColor='text-secondary' textSize='text-2xl' />
    )

    return (
      <div className='flex justify-center'>
        <Icon type='pet' fill textColor='text-secondary' textSize='text-2xl' />
        <input
          type='text'
          placeholder='Pet name'
          value={displayedPetName}
          readOnly={!canChange}
          onChange={onChange}
          ref={this.setInputElementRef}
          className='text-center bg-primary text-primary focus:outline-none focus:ring-none focus:border-transparent disabled:opacity-50'
        />
        <button onClick={this.handleRenameClick}>
          {editButton}
        </button>
      </div>
    )
  }
}

export default PetName
