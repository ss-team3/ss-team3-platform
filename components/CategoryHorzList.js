import { Component } from 'react'
import ScrollMenu from 'react-horizontal-scrolling-menu'
import Link from 'next/link'
import Avatar from './Avatar'

class CategoryHorzList extends Component {
  getColor (articles) {
    const l = articles.length
    if (l <= 3) return 'bg-blue-100'
    if (l <= 6) return 'bg-blue-200'
    if (l <= 9) return 'bg-blue-300'
    else return 'bg-blue-400'
  }

  getNameComponents (name) {
    const words = name.split(' ')
    if (words.length === 1) {
      return <span>{words[0]}</span>
    } else if (words.length === 2) {
      return [<span key={0}>{words[0]}</span>, <span key={1}>{words[1]}</span>]
    } else {
      return [<span key={0}>{words[0]}</span>, <span key={1}>{words[1]}...</span>]
    }
  }

  render () {
    const { categories } = this.props
    const { data } = categories
    const menuItems = data.map(datum => {
      const { name, articles, _id } = datum
      const href = `/category/${name === 'All' ? 'All' : _id}`

      return (
        <Link href={href} key={name}>
          <div className='w-min flex flex-col items-center px-1 my-2 sm:my-1 '>
            <Avatar
              type='text'
              size='w-11 h-11'
              className={this.getColor(articles)}
              text={name}
            />
            <div className='w-min flex flex-row flex-wrap justify-center items-center text-secondary text-xs text-center'>
              {this.getNameComponents(name)}
            </div>
          </div>
        </Link>
      )
    })

    return (
      <div>
        <ScrollMenu
          alignCenter={false}
          menuStyle={{ display: 'block' }}
          menuClass='my-3'
          innerWrapperClass='flex'
          itemClass='px-1'
          data={menuItems}
        />
      </div>
    )
  }
}

export default CategoryHorzList
