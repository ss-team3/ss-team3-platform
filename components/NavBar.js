import { Component } from 'react'
import { withRouter } from 'next/router'
import Link from 'next/link'
import Icon from './Icon'

class NavBar extends Component {
  constructor (props) {
    const { router } = props
    const { pathname } = router
    super(props)

    // FIXME: smell code
    const initSelected = this.parsePathName(pathname)
    this.state = {
      tabNames: ['home', 'category', 'pet', 'profile'],
      selected: initSelected,
    }

    this.handleRouteChange = this.handleRouteChange.bind(this)
    this.handleChangeSelected = this.handleChangeSelected.bind(this)
    this.generateTabComponent = this.generateTabComponent.bind(this)
  }

  componentDidMount () {
    this.props.router.events.on('routeChangeComplete', this.handleRouteChange)
  }

  componentWillUnmount () {
    // If the component is unmounted, unsubscribe
    // from the event with the `off` method:
    this.props.router.events.off('routeChangeComplete', this.handleRouteChange)
  }

  parsePathName (url) {
    return url.split('/')[1]
  }

  handleRouteChange (url, { shallow }) {
    const name = this.parsePathName(url)
    this.handleChangeSelected(name)
  }

  handleChangeSelected (name) {
    this.setState({ selected: name })
  }

  generateTabComponent (name) {
    const { selected } = this.state
    const isSelected = (selected === name)

    return (
      <Link href={`/${name}`} key={name}>
        <a
          className={`
            transition duration-300 ease-in-out
            w-full
            flex flex-col justify-center items-center
            ${isSelected ? 'bg-primary' : 'bg-secondary'}
          `}
          onClick={() => this.handleChangeSelected(name)}
        >
          <Icon
            type={name}
            textSize='text-3xl'
            textColor={`${isSelected ? 'text-secondary' : 'text-white'}`}
          />
          {isSelected
            ? <div className='text-sm text-secondary capitalize'>{name}</div>
            : ''}
        </a>
      </Link>
    )
  }

  render () {
    const { tabNames } = this.state
    const tabs = tabNames.map((name) => this.generateTabComponent(name))
    const height = NAVBAR_HEIGHT // fixed height of nav bar

    return (
      <nav className='w-full'>
        <div
          className={`fixed bottom-0 flex w-full ${height}`}
        >
          {tabs}
        </div>
      </nav>
    )
  }
}

const NAVBAR_HEIGHT = 'h-14'
export const NAVBAR_OFFSET = 'pb-14' // set offset by padding
export default withRouter(NavBar)
