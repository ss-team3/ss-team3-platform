import Head from 'next/head'
import React from 'react'

class HeadInfo extends React.Component {
  render () {
    return (
      <Head>
        <title>SS Team3 Platform</title>
        <link rel='icon' href='/favicon.ico' />

        {/* Google Fonts Icon */}
        {/* <!-- https://material.io/resources/icons/?style=baseline --> */}
        <link
          href='https://fonts.googleapis.com/css2?family=Material+Icons'
          rel='stylesheet'
        />

        {/* <!-- https://material.io/resources/icons/?style=outline --> */}
        <link
          href='https://fonts.googleapis.com/css2?family=Material+Icons+Outlined'
          rel='stylesheet'
        />

        {/* <!-- https://material.io/resources/icons/?style=round --> */}
        <link
          href='https://fonts.googleapis.com/css2?family=Material+Icons+Round'
          rel='stylesheet'
        />
      </Head>
    )
  }
}

export default HeadInfo
