import { Component } from 'react'
import InfiniteScroll from 'react-infinite-scroll-component'
import _ from 'lodash'

import Post from '../components/Post'

import { getCurrentPosts } from '../api/article'

class PostList extends Component {
  constructor (props) {
    super(props)
    this.state = {
      posts: {
        // total: number of  article user added, does not count duplicated article used for infinite scroll purpose
        total: 0,
        data: [],
      },
    }

    this.handleLoadMore = this.handleLoadMore.bind(this)
    this.handleRefresh = this.handleRefresh.bind(this)
  }

  async componentDidMount () {
    await this.handleRefresh()
  }

  async handleLoadMore () {
    let total = this.state.posts.total
    let data = [...this.state.posts.data]
    const offset = this.state.posts.data.length

    if (offset >= total) {
      const newData = _.sampleSize(data.slice(0, total), Math.min(total, 10)).map(item => { return { ...item } })
      this.attachKey(newData, true)
      data = data.concat(newData)
    } else {
      const { total: newTotal, data: newData } = await getCurrentPosts({ limit: 10, offset })
      this.attachKey(newData, false)
      total = newTotal
      data = data.concat(newData)
    }

    this.setState({ posts: { total, data } })
  }

  async handleRefresh () {
    const { total, data } = await getCurrentPosts({ limit: 10, offset: 0 })
    this.attachKey(data, false)
    this.setState({ posts: { total, data } })
  }

  attachKey (data, isDuplicated) {
    const generateFakeKey = () => (_.random(0x0000000000, 0xffffffffff).toString(16))
    for (const datum of data) {
      const fakeKey = generateFakeKey()
      datum._key = isDuplicated ? fakeKey : datum._id
    }
  }

  /**
   * @params {Object} props.topContent - content at the top of infiniteScroll
   */
  render () {
    const { props, state, handleLoadMore, handleRefresh } = this
    const { posts } = state
    const { total, data } = posts
    const { topContent } = props

    const postComponents = []
    for (const datum of data) {
      postComponents.push(
        <Post key={datum._key} data={datum} />,
      )
    }

    return (
      <InfiniteScroll
        className='flex flex-col gap-4 p-1'
        dataLength={data.length}
        next={handleLoadMore}
        hasMore
          // Pull down functionality
        refreshFunction={handleRefresh}
        pullDownToRefresh
        pullDownToRefreshThreshold={100}
        pullDownToRefreshContent={
          // FIXME: style 'pull to refresh'
          <p className='mt-8 text-center text-secondary text-lg'>&#8595; Pull down to refresh</p>
          }
        releaseToRefreshContent={
          <p className='mt-8 text-center text-secondary text-lg'>&#8593; Release to refresh</p>
          }
      >
        {/* Add -mt-4 to offset gap-4 between top Content & pullDown element */}
        <div className='-mt-4'>
          {topContent}
        </div>
        {postComponents}
      </InfiniteScroll>
    )
  }
}

export default PostList
