import { Component } from 'react'
import Icon from '../components/Icon'

/**
 * @param {String} type - icon.type
 * @param {Bool} fill - icon.fill
 * @param {Bool} iconColor - color of icon
 * @param {String} textColor - text  color of button
 * @param {String} bgColor - background color of button
 * @param {Function} onClick - click handler function
 * @param {Object} children - text of button
 */
class IconButton extends Component {
  render () {
    const { props } = this
    const { fill, type, iconColor, textColor, bgColor, onClick, children } = props
    return (
      <button
        onClick={onClick}
        className={`${textColor} ${bgColor} rounded-full border-2 border-secondary py-2 px-4 inline-flex items-center`}
      >
        <div className='w-7 h-7 inline-block mr-1'>
          <Icon type={type} fill={fill} textColor={iconColor} />
        </div>
        {children}
      </button>
    )
  }
}

// For hover:
// hover:border-transparent hover:text-background hover:bg-primary
export default IconButton
