export default function DummyPage ({ pageName = 'dummy' }) {
  return (
    <div className='w-screen h-screen flex flex-col justify-between items-center'>
      <p className='w-full h-full text-6xl text-secondary flex justify-center items-center'>
        {pageName}
      </p>
      <footer className='mb-4 text-base text-primary'>
        <a
          href='https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app'
          target='_blank'
          rel='noopener noreferrer'
        >
          Powered by Pokcat team
        </a>
      </footer>
    </div>
  )
}
