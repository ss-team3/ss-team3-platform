import { Component } from 'react'

/**
 * @param {String} textColor - text color of button
 * @param {String} bgColor - background color of button
 * @param {String} className - background color of button
 * @param {Function} onClick - click handler function
 * @param {Object} children - text of button
 */
class Button extends Component {
  render () {
    const { props } = this
    const { textColor = 'text-white', bgColor = 'bg-secondary', children, onClick, className = '' } = props
    return (
      <button
        className={`${textColor} ${bgColor} ${className} py-2 px-4 rounded-full shadow-md`}
        type='button'
        onClick={onClick}
      >
        {children}
      </button>
    )
  }
}

export default Button
