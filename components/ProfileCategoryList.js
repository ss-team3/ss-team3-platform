import { Component } from 'react'
import Link from 'next/link'
import Icon from './Icon'
import CategoryHorzList from './CategoryHorzList'

/**
 * @param {Object} props.type - category list
 */
export default class ProfileCategoryList extends Component {
  render () {
    const { categories } = this.props
    return (
      <div className='flex flex-col justify-center my-4'>
        <div className='w-full flex flex-row items-center justify-between'>
          <p className='text-xl font-bold'>Categories</p>
          <Link href='/profile/categoriesSetting'>
            <button className='flex flex-row items-center border-2 border-secondary px-2 rounded-full shadow-sm'>
              <p className='text-secondary px-2'>Edit</p>
              <Icon fill type='edit' textColor='text-secondary' textSize='text-2xl' />
            </button>
          </Link>
        </div>

        <CategoryHorzList categories={categories} />
      </div>
    )
  }
}
