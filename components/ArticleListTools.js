import { Component } from 'react'
import Icon from '../components/Icon'

class ArticleListTools extends Component {
  render () {
    const { props } = this
    const { articlesTypes, ascendingByTime, onBookmarkClick, onArchiveClick, onTimeClick } = props
    return (
      <>
        <div className='flex flex-row gap-2 justify-end'>
          <div onClick={onBookmarkClick}>
            <Icon
              type='bookmark'
              fill={articlesTypes.includes('bookmarked')}
              textColor='text-secondary'
              textSize='text-3xl'
            />
          </div>
          <div onClick={onArchiveClick}>
            <Icon
              type='archive'
              fill={articlesTypes.includes('archived')}
              textColor='text-secondary'
              textSize='text-3xl'
            />
          </div>
          <div onClick={onTimeClick}>
            <Icon
              type={ascendingByTime ? 'arrowDown' : 'arrowUp'}
              textColor='text-secondary'
              textSize='text-3xl'
            />
          </div>
        </div>
      </>
    )
  }
}

export default ArticleListTools
