import { Component } from 'react'
import Icon from './Icon'

/**
 * @param {String} props.content - Modal's content in string
 * @param {Boolean} props.warningIcon - whether Modal has warning icon
 * @param {String} props.leftButton - content of left Button
 * @param {String} props.rightButton - content of right Button
 * @param {Callback} props.onRightOrLeftButtonClick - function to handle leftButton or rightButton click
 */
class Modal extends Component {
  render () {
    const { props } = this
    const { content = 'Make sure you want to delete this.', warningIcon = false, leftButton = 'Delete', rightButton = 'Cancel', onRightButtonClick, onLeftButtonClick } = props
    const icon = warningIcon ? <Icon type='warning' textColor='text-red-500' textSize='text-4xl' /> : <></>

    return (
      <div className='border-2 border-secondary rounded'>
        <div className='px-4 py-7 flex flex-row items-center gap-2 bg-primary text-primary rounded-t'>
          {icon}
          <p className='text-lg'>
            {content}
          </p>
        </div>
        {/* check button */}
        <div className='flex justify-end items-center p-4 bg-primary border-t border-solid border-text-primary rounded-b'>
          <button
            className={`text-red-500 font-bold uppercase px-6 py-2 text-sm rounded outline-none shadow hover:shadow-lg focus:outline-none mr-1 mb-1 ${leftButton === '' ? 'hidden' : 'block'}`}
            onClick={onLeftButtonClick}
          >
            {leftButton}
          </button>
          <button
            className='text-primary font-bold uppercase px-6 py-2 text-sm rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1'
            onClick={onRightButtonClick}
          >
            {rightButton}
          </button>
        </div>
      </div>
    )
  }
}

export default Modal
