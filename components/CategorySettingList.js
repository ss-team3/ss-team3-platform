import { Component } from 'react'
import CategorySettingListItem from './CategorySettingListItem'

class CategorySettingList extends Component {
  render () {
    const { props } = this
    const { categories, onCategoryRename, onCategoryRemove } = props
    const { data } = categories
    const categoryComponents = []

    for (const datum of data) {
      // Cannot modify default category
      if (datum.name === 'All') continue

      const { _id, articles } = datum
      categoryComponents.push(
        <CategorySettingListItem
          key={_id}
          data={datum}
          onCategoryRename={onCategoryRename}
          onCategoryRemove={onCategoryRemove}
          totalArticles={articles.length}
        />,
      )
    }
    return (
      <div className='w-full flex flex-col gap-4 items-center'>
        {categoryComponents}
      </div>
    )
  }
}

export default CategorySettingList
