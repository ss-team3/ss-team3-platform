import { Component } from 'react'
import _ from 'lodash'
import ArticleListTools from './ArticleListTools'
import ArticleListItem from './ArticleListItem'

class ArticleList extends Component {
  constructor (props) {
    super(props)
    this.state = {
      articlesTypes: [], // 'bookmarked', 'archived'
      ascendingByTime: true,
    }
    this.handleBookmarkClick = this.handleBookmarkClick.bind(this)
    this.handleArchiveClick = this.handleArchiveClick.bind(this)
    this.handleTimeClick = this.handleTimeClick.bind(this)
    this.updateArticlesTypes = this.updateArticlesTypes.bind(this)
  }

  handleTimeClick (e) {
    this.setState(() => {
      return { ascendingByTime: !this.state.ascendingByTime }
    })
  }

  handleArchiveClick (e) {
    this.setState((prevState) => {
      const articlesTypes = this.updateArticlesTypes(prevState, 'archived')
      return { articlesTypes }
    })
  }

  handleBookmarkClick (e) {
    this.setState((prevState) => {
      const articlesTypes = this.updateArticlesTypes(prevState, 'bookmarked')
      return { articlesTypes }
    })
  }

  updateArticlesTypes (prevState, type) {
    const articlesTypes = [...prevState.articlesTypes] // shallow copy
    const indexOfType = prevState.articlesTypes.indexOf(type)
    if (indexOfType === -1) {
      articlesTypes.push(type)
    } else {
      articlesTypes.splice(indexOfType, 1)
    }
    return articlesTypes
  }

  render () {
    const { props, state } = this
    const { articlesTypes, ascendingByTime } = state
    const { data, onChangeBookmarkArchived } = props
    const flexDirection = ascendingByTime ? 'flex-col' : 'flex-col-reverse'
    let selectedArticles = _.orderBy(data, ['estimated_time'], ['asc'])

    if (articlesTypes.includes('bookmarked')) {
      selectedArticles = selectedArticles.filter(article => article.bookmark)
    }
    selectedArticles = selectedArticles.filter(article => article.archived === articlesTypes.includes('archived'))

    const articlesComponents = selectedArticles.map(article =>
      <ArticleListItem
        key={article._id}
        article={article}
        onChangeBookmarkArchived={onChangeBookmarkArchived}
      />,
    )

    return (
      <>
        <ArticleListTools
          articlesTypes={articlesTypes}
          ascendingByTime={ascendingByTime}
          onBookmarkClick={this.handleBookmarkClick}
          onArchiveClick={this.handleArchiveClick}
          onTimeClick={this.handleTimeClick}
        />
        <div className={`flex ${flexDirection} items-start gap-4`}>
          {articlesComponents}
        </div>
      </>
    )
  }
}

export default ArticleList
