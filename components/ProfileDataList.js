import { Component } from 'react'
/**
 * @param {Number} props.type - totalReadTime
 * @param {Number} props.type - totalReadArticle
 * @param {Number} props.type - targetReadArticle
 * @param {Number} props.type - numOfBookmark
 */

export default class ProfileDataList extends Component {
  render () {
    const {
      totalReadTime = 0,
      targetReadArticle = 0,
      weekAlreadyRead = 0,
      numOfBookmark = 0,
    } = this.props
    const readPercent = weekAlreadyRead / targetReadArticle
    const readPercentStyles = { width: `${readPercent * 100}%` }
    const unreadPercentStyles = { width: `${(1 - readPercent) * 100}%` }

    return (
      <>
        <div className='flex flex-row w-full justify-center'>
          <div className='w-24 h-11 text-center'>
            <p className='text-lg text-blue-300'>{totalReadTime}</p>
            <p className='font-semibold text-secondary'>min</p>
          </div>
          <div className='w-24 h-11 text-center'>
            <p className='text-lg text-pink-300'>{`${weekAlreadyRead}/${targetReadArticle}`}</p>
            <p className='font-semibold text-secondary'>read</p>
          </div>
          <div className='w-24 h-11 text-center'>
            <p className='text-lg text-green-300'>{numOfBookmark}</p>
            <p className='font-semibold text-secondary'>bookmark</p>
          </div>
        </div>
        <div className='w-full h-1 border-0 border-secondary flex flex-row'>
          <div className='h-full bg-pink-200 shadow-inner' style={readPercentStyles} />
          <div className='h-full bg-gray-100 shadow-inner' style={unreadPercentStyles} />
        </div>
      </>
    )
  }
}
