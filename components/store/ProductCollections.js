import { Component } from 'react'
import CollectionItem from './CollectionItem'
import { getProducts, buyProduct } from '../../api/store'

class ProductCollections extends Component {
  constructor (props) {
    super(props)
    this.state = {
      currentProducts: [],
      products: [],
      selected: 'Foods',
      types: ['Foods'],
    }
    this.initAPI = this.initAPI.bind(this)
    this.handleBuyProduct = this.handleBuyProduct.bind(this)
  }

  async initAPI () {
    const { data: products } = await getProducts()
    const types = [...new Set(products.map(item => item.type))]
    this.setState({
      products,
      types,
      selected: types[0],
    })
  }

  async componentDidMount () {
    await this.initAPI()
  }

  async handleChangeSelected (name) {
    this.setState({ selected: name })
  }

  async handleBuyProduct (userMoney, price, _id, name) {
    if (userMoney < price) {
      this.props.onHandleModal('你的錢不夠哦')
      return
    }
    const result = await buyProduct({ _id: _id })
    await this.props.onChangeUserMoney()
    if (result.success === true) {
      this.props.onHandleModal(`購買 ${name} !`)
    }
  }

  render () {
    const { props } = this
    const { userMoney } = props
    const typeComponents = []
    const productComponents = []

    for (const type of this.state.types) {
      typeComponents.push(
        <button
          key={type}
          className={`w-full py-4 hover:text-primary focus:outline-none ${type === this.state.selected ? 'text-primary border-b-2 font-medium border-secondary' : 'text-text '} `}
          onClick={() => this.handleChangeSelected(type)}
        >
          {type}
        </button>)
    }

    for (const datum of this.state.products) {
      if (datum.type === this.state.selected) {
        productComponents.push(
          <div className='h-52' key={datum._id}>
            <CollectionItem
              product={datum}
              userMoney={userMoney}
              onClickItem={this.handleBuyProduct}
            />
          </div>,
        )
      }
    }

    return (
      <>
        <nav className='flex flex-row flex-1 justify-around mb-5'>
          {typeComponents}
        </nav>
        <div className='grid grid-cols-2 md:grid-cols-3 gap-5'>
          {productComponents}
        </div>
      </>
    )
  }
}

export default ProductCollections
