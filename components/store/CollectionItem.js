import { Component } from 'react'
import Icon from '../Icon'

/**
 * @param {Object} product
 * @param {Number} userMoney - user's money
 * @param {Function} onClickItem - buy when click item
 */
class CollectionItem extends Component {
  render () {
    const { props } = this
    const { product, userMoney } = props
    // eslint-disable-next-line camelcase
    const { name, price, _id, energy } = product
    return (
      <button
        onClick={() => this.props.onClickItem(userMoney, price, _id, name)}
        className='h-full w-full pb-2 flex flex-col justify-between overflow-hidden rounded-2xl shadow-md hover:shadow-lg'
      >
        <div className='h-2/3 w-full'>
          {/* eslint-disable-next-line camelcase */}
          <img
            src={`/product/${name}.png`}
            alt={name}
            className='object-contain h-full w-full border-secondary border-b'
          />
        </div>
        <div className='h-1/3 pt-2 w-full flex-grow flex flex-col justify-around gap-1'>
          <div className='text-xl'>
            {name}
          </div>
          <div className='flex flex-row justify-around items-center'>
            <div className='text-lg text-secondary flex flex-row items-center'>
              <Icon type='money' textColor='text-secondary' textSize='text-lg' />
              {price}
            </div>
            <div className='text-lg text-blue-300 flex flex-row items-center gap-1'>
              <Icon type='tableware' textColor='text-blue-300' textSize='text-lg' />
              {energy}
            </div>
          </div>
        </div>
      </button>
    )
  }
}

export default CollectionItem
