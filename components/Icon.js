/**
 * @property {<path />} path - svg path
 * @property {stroke-current|fill-current} colorMethod - svg icon 分成使用 stroke或 fill 上色，填錯會倒致無法正確顯示顏色
 */
const iconObj = {
  bookmark: {
    nonFill: <span className='material-icons-outlined'> bookmark_border </span>,
    fill: <span className='material-icons-outlined'> bookmark </span>,
  },
  add: {
    nonFill: <span className='material-icons-outlined'> add </span>,
    fill: <span className='material-icons'> add </span>,
  },
  chat: {
    nonFill: <span className='material-icons-outlined'> chat_bubble_outline </span>,
    fill: <span className='material-icons-outlined'> chat_bubble </span>,
  },
  category: {
    nonFill: <span className='material-icons-outlined'> category </span>,
    fill: <span className='material-icons'> category </span>,
  },
  profile: {
    nonFill: <span className='material-icons-outlined'> person </span>,
    fill: <span className='material-icons'> person </span>,
  },
  home: {
    nonFill: <span className='material-icons-outlined'> home </span>,
    fill: <span className='material-icons'> home </span>,
  },
  time: {
    nonFill: <span className='material-icons-outlined'> watch_later </span>,
    fill: <span className='material-icons'> watch_later </span>,
  },
  search: {
    nonFill: <span className='material-icons-outlined'> search </span>,
    fill: <span className='material-icons-outlined'> search </span>,
  },
  dropdown: {
    nonFill: <span className='material-icons'> expand_more </span>,
    fill: <span className='material-icons'> expand_more </span>,
  },
  archive: {
    nonFill: <span className='material-icons-outlined'> archive </span>,
    fill: <span className='material-icons'> archive </span>,
  },
  pet: {
    nonFill: <span className='material-icons-outlined'> pets </span>,
    fill: <span className='material-icons-outlined'> pets </span>,
  },
  heart: {
    nonFill: <span className='material-icons-outlined'> favorite_border </span>,
    fill: <span className='material-icons-outlined'> favorite </span>,
  },
  tableware: {
    nonFill: <span className='material-icons'> restaurant </span>,
    fill: <span className='material-icons'> restaurant </span>,
  },
  navBack: {
    nonFill: <span className='material-icons-outlined'> navigate_before </span>,
    fill: <span className='material-icons-outlined'> navigate_before </span>,
  },
  readMore: {
    nonFill: <span className='material-icons-outlined'> navigate_next </span>,
    fill: <span className='material-icons-outlined'> navigate_next </span>,
  },
  audio: {
    nonFill: <span className='material-icons-outlined'> play_circle </span>,
    fill: <span className='material-icons-outlined'> play_circle_filled </span>,
  },
  setting: {
    nonFill: <span className='material-icons-outlined'> tune </span>,
    fill: <span className='material-icons-outlined'> tune </span>,
  },
  settingColor: {
    nonFill: <span className='material-icons-outlined'> fiber_manual_record </span>,
    fill: <span className='material-icons-outlined'> fiber_manual_record </span>,
  },
  settingNightMode: {
    nonFill: <span className='material-icons-outlined'> brightness_6 </span>,
    fill: <span className='material-icons'> brightness_6 </span>,
  },
  email: {
    nonFill: <span className='material-icons-outlined'> email </span>,
    fill: <span className='material-icons'> email </span>,
  },
  delete: {
    nonFill: <span className='material-icons-outlined'> delete </span>,
    fill: <span className='material-icons'> delete </span>,
  },
  navNext: {
    nonFill: <span className='material-icons-outlined'> navigate_next </span>,
    fill: <span className='material-icons-outlined'> navigate_next </span>,
  },
  shoppingCart: {
    nonFill: <span className='material-icons-outlined'>shopping_cart</span>,
    fill: <span className='material-icons'>shopping_cart</span>,
  },
  edit: {
    nonFill: <span className='material-icons-outlined'>create</span>,
    fill: <span className='material-icons'>create</span>,
  },
  done: {
    nonFill: <span className='material-icons'>done_outline</span>,
    fill: <span className='material-icons'>done</span>,
  },
  money: {
    nonFill: <span className='material-icons'>attach_money</span>,
    fill: <span className='material-icons'>attach_money</span>,
  },
  arrowDown: {
    nonFill: <span className='material-icons-outlined'> arrow_downward </span>,
    fill: <span className='material-icons-outlined'> arrow_downward </span>,
  },
  arrowUp: {
    nonFill: <span className='material-icons-outlined'> arrow_upward </span>,
    fill: <span className='material-icons-outlined'> arrow_upward </span>,
  },
  remove: {
    notFill: <span className='material-icons-outlined'>remove_circle_outline</span>,
    fill: <span className='material-icons-outlined'>remove_circle</span>,
  },
  addPhoto: {
    notFill: <span className='material-icons-outlined'>add_a_photo</span>,
    fill: <span className='material-icons-outlined'>add_a_photo</span>,
  },
  smile: {
    nonFill: <span className='material-icons-outlined'>sentiment_satisfied_alt</span>,
    fill: <span className='material-icons-round'>emoji_emotions</span>,
  },
  warning: {
    nonFill: <span className='material-icons-outlined'> error_outline </span>,
    fill: <span className='material-icons-outlined'> error </span>,
  },
}

/**
* @param {String} props.type - name of icon
* @param {Bool}  props.fill - fill the icon (only support some icons)
* @param {String} props.textColor - tailwind text color properties
* @param {String} props.textSize - tailwind text size properties
* @param {String} props.className - other tailwind properties
*/
function Icon (props) {
  const { type, fill = false, textColor = 'text-secondary', textSize = 'text-4xl', className = '', style = {} } = props

  if (!iconObj[type] || !((fill) ? iconObj[type].fill : iconObj[type].nonFill)) {
    console.error(`Invalid icon. type: '${type}', fill: '${fill}'`)
    throw Error(`Invalid icon. type: '${type}', fill: '${fill}'`)
  }

  const icon = (fill) ? iconObj[type].fill : iconObj[type].nonFill
  return (
    <span className={`${icon.props.className} ${textColor} ${textSize} ${className} flex justify-center items-center`} style={style}>
      {icon.props.children}
    </span>
  )
}

export default Icon
