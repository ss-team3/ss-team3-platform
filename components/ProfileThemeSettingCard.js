import { Component } from 'react'

/**
 * @param {Object} props.type - category list
 */
export default class ProfileThemeSettingCard extends Component {
  render () {
    const { onChangeTheme, theme } = this.props

    const generateRadio = (targetTheme) => {
      if (theme !== targetTheme) {
        return (
          <button
            onClick={() => onChangeTheme(targetTheme)}
            className='w-4 h-4 m-2 bg-secondary opacity-50 shadow-inner rounded-full'
          />
        )
      } else {
        return (
          <div className='w-4 h-4 m-2 bg-secondary shadow-inner rounded-full'>
            <div className='w-2 h-2 m-1 bg-white shadow-inner rounded-full' />
          </div>
        )
      }
    }

    return (
      <div className='w-full flex flex-row items-center justify-between my-2'>
        <p className='text-xl text-primary'>App Theme</p>
        <div className='items-center text-primary'>
          <div className='flex flex-row items-center'>
            {generateRadio('light')}
            Light
            {generateRadio('dark')}
            Dark
          </div>
        </div>
      </div>

    )
  }
}
