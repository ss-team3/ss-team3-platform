import { Component } from 'react'

/**
 * @param {(image|text)} props.type - type of Avatar
 * @param {String} props.url - image url for `image` type
 * @param {String} props.text - text for `text` type
 * @param {String} props.size - avatar size class (e.g. w-5, h-5)
 * @param {String?} props.className - other style class, should not contain size class (e.g. text-base bg-blue-300...)
 * @param {String?} props.backgroundHex - (optional) hex background color (e.g. #123456)
 */
class Avatar extends Component {
  render () {
    const { props } = this
    const { type, size, url, text, backgroundHex, className = '' } = props
    let style = {}

    if (!type || !size) {
      throw new Error('Insufficient props, avatar require `type` and `size` props')
    }

    if (backgroundHex) {
      style = { backgroundColor: backgroundHex }
    }

    if (type === 'image') {
      // if (!url) { throw new Error('Insufficient props, image type avatar require `url` props') }

      if (!url) {
        return (
          <div className={`${size} animate-pulse`}>
            <div className={`${className} bg-secondary w-full h-full rounded-full flex justify-center items-center`} style={style} />
          </div>
        )
      }

      return (
        <div className={`${size} ${className} rounded-full flex justify-center items-center`} style={style}>
          <img className='inline-block rounded-full ring-0' src={url} />
        </div>
      )
    } else if (type === 'text') {
      if (!text) { throw new Error('Insufficient props, text type avatar require `text` props') }

      const formatText = (text) => {
        // split text to word by space
        const words = text.split(' ', 2)
        let res = ''
        if (words.length < 2) {
          // use word's first two char
          res = words[0].slice(0, 2)
        } else {
          // join first two words' first char
          res = words.map((word) => word[0]).join('')
        }
        // turn into uppercase
        return res.toUpperCase()
      }
      const formattedText = formatText(text)

      return (
        <div className={size}>
          <div
            className={`${className} text-white font-bold w-full h-full uppercase rounded-full flex justify-center items-center`}
            style={style}
          >
            {formattedText}
          </div>
        </div>
      )
    }
  }
}

export default Avatar
