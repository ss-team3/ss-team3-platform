import { Component } from 'react'
import NavBackButton from './NavBackButton'

/**
 * @param {String} props.title - title show on the left size
 * @param {Boolean} props.canNavBack - enable navBack indicator
 * @param {Object} props.utils - utils on the right side
 */
class HeaderBar extends Component {
  constructor (props) {
    super(props)
    this.state = {}
  }

  render () {
    const { props } = this
    const { title, canNavBack = false, leftUtils = '', rightUtils = '' } = props
    const height = HEADERBAR_HEIGHT // fixed height of header bar
    if (title && leftUtils) throw new Error('Should not use `title` and `leftUtils` in HeaderBar at the same time, would cause UI conflict')

    const titleComponent = (
      <h1 className='text-3xl select-none'>
        {title}
      </h1>
    )

    return (
      <div className='w-full'>
        <div className={`
            fixed z-50
            w-full ${height}
            px-4 md:px-6
            bg-primary shadow-sm
            text-primary
            flex flex-row justify-between items-center
          `}
        >
          <div className='justify-items-start flex flex-row items-center gap-3'>
            {canNavBack && <NavBackButton />}
            {title && titleComponent}
            {leftUtils}
          </div>
          <div className='justify-items-end flex flex-row items-center gap-3'>
            {rightUtils}
          </div>
        </div>
      </div>
    )
  }
}

const HEADERBAR_HEIGHT = 'h-14'
export const HEADERBAR_OFFSET = 'pt-14' // set offset by padding
export default HeaderBar
