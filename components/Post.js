import { Component } from 'react'
import Link from 'next/link'

import Icon from './Icon'
import Avatar from './Avatar'

import { modifyCurrentArticle } from '../api/article'

class Post extends Component {
  constructor (props) {
    super(props)
    this.state = {
      isBookmarked: props.data.bookmark,
    }

    this.handleBookmarkClick = this.handleBookmarkClick.bind(this)
  }

  handleBookmarkClick (e) {
    // Prevent click event propagate to article link
    e.preventDefault()

    const { props, state } = this
    const { data } = props
    this.setState({ isBookmarked: !state.isBookmarked }, async () => {
      await modifyCurrentArticle({ _id: data._id, bookmark: this.state.isBookmarked })
    })
  }

  render () {
    const { isBookmarked } = this.state
    const { data } = this.props
    const { _id, title, estimated_time: estimatedTime, cover_url: coverUrl, category } = data
    const textContainerMx = 'mx-3'

    return (
      <div className='flex flex-col py-3 rounded-2xl shadow-md'>

        {/* Link to category page */}
        <Link href={`/category/${category._id}`}>
          <a>
            {/* Post header */}
            <div className={`flex justify-start items-center ${textContainerMx} py-2 space-x-3`}>
              <Avatar
                type='text'
                size='w-8 h-8'
                text={category.name}
                className='text-base bg-blue-300'
              />
              <p className='text-lg font-medium'>{category.name}</p>
            </div>
          </a>
        </Link>

        {/* Link to article page */}
        <Link href={`/article/${_id}`}>
          <a>
            {/* Post cover */}
            <div className='bg-gray-300'>
              <img className='object-cover w-full h-96' src={coverUrl} alt={coverUrl} />
            </div>

            {/* Post title */}
            <div className={textContainerMx}>
              <p className='py-3 text-2xl lg:text-3xl font-medium'>{title}</p>
            </div>

            {/* Post utils */}
            <div className={textContainerMx}>
              <div className='flex flex-row justify-between items-center'>
                <div className='flex justify-start items-center gap-2'>
                  <Icon type='time' textColor='text-secondary' textSize='text-2xl' />
                  <p className='text-secondary text-lg'>{estimatedTime} mins</p>
                </div>
                <div onClick={this.handleBookmarkClick}>
                  <Icon type='bookmark' textColor='text-secondary' textSize='text-2xl' fill={!!isBookmarked} />
                </div>
              </div>
            </div>
          </a>
        </Link>
      </div>
    )
  }
}

export default Post
