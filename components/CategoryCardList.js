import { Component } from 'react'
import CategoryCardItem from './CategoryCardItem'

class CategoryCardList extends Component {
  render () {
    return (
      <div className='w-full grid grid-cols-2 gap-2'>
        <CategoryCardItem type='quick' />
        <CategoryCardItem type='short' />
        <CategoryCardItem type='long' />
        <CategoryCardItem type='focus' />
      </div>
    )
  }
}

export default CategoryCardList
