import { HEADERBAR_OFFSET } from './HeaderBar'
import { NAVBAR_OFFSET } from './NavBar'

function Layout ({ shouldFullScreen, shouldPadHeaderBar, shouldPadNavBar, children }) {
  return (
    <div
      className={`
        ${shouldFullScreen ? 'max-h-screen h-screen justify-center' : ''}
        ${shouldPadHeaderBar ? HEADERBAR_OFFSET : ''}
        ${shouldPadNavBar ? NAVBAR_OFFSET : ''}
        flex flex-col items-center bg-primary text-primary min-h-screen
      `}
    >
      <div
        className={`
          px-4 md:px-0 py-4
          w-full max-w-screen-sm
        `}
      >
        {children}
      </div>
    </div>
  )
}

export default Layout
