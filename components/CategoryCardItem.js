import { Component } from 'react'
import Link from 'next/link'
/**
 * @param {String} props.type - quick, short, long, or focus
 */

class CategoryCardItem extends Component {
  getTimeInterval (type) {
    if (type === 'quick') return '< 5'
    else if (type === 'short') return '5 - 10'
    else if (type === 'long') return '10 - 20'
    else if (type === 'focus') return '> 20'
    else return 'wrong time category'
  }

  getColor (type) {
    if (type === 'quick') return 'bg-pink-200 hover:bg-pink-400 text-pink-600 hover:text-white '
    else if (type === 'short') return 'bg-yellow-200 hover:bg-yellow-400 text-yellow-600 hover:text-white '
    else if (type === 'long') return 'bg-green-200 hover:bg-green-400 text-green-600 hover:text-white '
    else if (type === 'focus') return 'bg-blue-200 hover:bg-blue-400 text-blue-600 hover:text-white '
    else return 'bg-gray-200 hover:bg-gray-400 text-gray-600 hover:text-white '
  }

  render () {
    const { type } = this.props
    const color = this.getColor(type)
    return (
      <Link href={`/category/${type}`}>
        <button className={`${color} rounded-lg px-4 py-4 text-left md:text-center leading-loose shadow-md`}>
          <p className='text-xl align-top'>{type.replace(/^[a-z]/i, str => str.toUpperCase())}</p>
          <p className='text-sm align-bottom'>{`${this.getTimeInterval(type)} mins`}</p>
        </button>
      </Link>

    )
  }
}

export default CategoryCardItem
