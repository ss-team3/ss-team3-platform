import api from '../lib/request'

export function getOwnedProducts (params) {
  return api('/ownedProducts/getOwnedProducts', params)
}

export function useOwnedProduct (params) {
  return api('/ownedProducts/useOwnedProduct', params)
}
