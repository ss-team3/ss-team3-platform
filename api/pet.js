import api from '../lib/request'

export function getCurrentPet () {
  return api('/pets/getCurrentPet')
}

export function modifyCurrentPet (params) {
  return api('/pets/modifyCurrentPet', params)
}
