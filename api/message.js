import api from '../lib/request'

export function addCurrentMessage (params) {
  return api('/message/addCurrentMessage', params)
}

export function getCurrentMessages (params) {
  return api('/message/getCurrentMessages', params)
}
