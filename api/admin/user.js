import api from '../../lib/request'

export function addUser (params) {
  return api('/admin/users/addUser', params)
}

export function getUser (params) {
  return api('/admin/users/getUser', params)
}

export function getUsers (params) {
  return api('/admin/users/getUsers', params)
}

export function modifyUser (params) {
  return api('/admin/users/modifyUser', params)
}

export function removeUser (params) {
  return api('/admin/users/removeUser', params)
}
