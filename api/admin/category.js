import api from '../../lib/request'

export function getCategoriesForAdmin (params) {
  return api('/admin/categories/getCategories', params)
}
