import api from '../../lib/request'

export function getArticle (params) {
  return api('/admin/articles/getArticle', params)
}

export function getArticles (params) {
  return api('/admin/articles/getArticles', params)
}

export function modifyArticle (params) {
  return api('/admin/articles/modifyArticle', params)
}

export function removeArticle (params) {
  return api('/admin/articles/removeArticle', params)
}
