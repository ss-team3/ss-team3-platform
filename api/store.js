import api from '../lib/request'

export function getProducts (params) {
  return api('/products/getProducts', params)
}

export function buyProduct (params) {
  return api('/products/buyProduct', params)
}
