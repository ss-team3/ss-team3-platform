import api from '../lib/request'

export function addCurrentCategory (params) {
  return api('/categories/addCurrentCategory', params)
}

export function getCurrentCategory (params) {
  return api('/categories/getCurrentCategory', params)
}

export function getCurrentCategories (params) {
  return api('/categories/getCurrentCategories', params)
}

export function modifyCurrentCategory (params) {
  return api('/categories/modifyCurrentCategory', params)
}

export function removeCurrentCategory (params) {
  return api('/categories/removeCurrentCategory', params)
}
