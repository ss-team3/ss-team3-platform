import api from '../lib/request'

export function addCurrentArticle (params) {
  return api('/articles/addCurrentArticle', params)
}

export function getCurrentArticle (params) {
  return api('/articles/getCurrentArticle', params)
}

export function getCurrentArticles (params) {
  return api('/articles/getCurrentArticles', params)
}

export function getCurrentPosts (params) {
  return api('/articles/getCurrentPosts', params)
}

export function modifyCurrentArticle (params) {
  return api('/articles/modifyCurrentArticle', params)
}

export function removeCurrentArticle (params) {
  return api('/articles/removeCurrentArticle', params)
}
