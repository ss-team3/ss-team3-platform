import api from '../lib/request'

export function signInSignUp (token) {
  return api('/users/signInSignUp', token)
}

export function getCurrentUser () {
  return api('/users/getCurrentUser')
}

export function modifyCurrentUser (params) {
  return api('/users/modifyCurrentUser', params)
}
