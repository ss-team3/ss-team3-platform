import { AuthContext } from '../components/providers/AuthProvider'
import { useContext, useEffect } from 'react'
import Router from 'next/router'

/**
 * @returns - refer to AuthProvider.js
 */
export function useAuth ({
  shouldAuth = true,
  redirectTo = '/login',
} = {}) {
  const auth = useContext(AuthContext)
  const { isLogin } = auth
  useEffect(() => {
    // if firebase user haven't ready, do nothing
    if (typeof isLogin === 'undefined') return
    if (shouldAuth !== isLogin) { Router.push(redirectTo) }
  }, [auth])
  return auth
}
