import Axios from 'axios'

const axiosInstance = Axios.create({
  baseURL: `${process.env.NEXT_PUBLIC_BACKEND_HOST}/`,
  timeout: 5000,
})

// request interceptors
axiosInstance.interceptors.request.use((config) => config, (error) => {
  // console.log('[Axios]: request error')
  return Promise.reject(error)
})

// response interceptors
axiosInstance.interceptors.response.use((response) => {
  // console.log('[Axios]: get response')
  return response.data
}, (error) => {
  const { response = {} } = error
  const { data = null } = response
  // console.log('error = ', data) // for debug
  return Promise.reject(data || error)
})

export default async function api (url, data, method = 'POST', params = {}) {
  const headers = {}
  let token = ''
  if (typeof window !== 'undefined') {
    token = window.localStorage.getItem('auth_token')
  }
  if (token) {
    headers.Authorization = `Bearer ${token}`
    headers['Access-Control-Allow-Origin'] = '*'
  }

  const result = await axiosInstance.request({
    method,
    url,
    data,
    params,
    headers,
  })
  return result
}
