import firebase from 'firebase/app'
import 'firebase/auth'

// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: 'AIzaSyA9LKAyf8osO_Jfy1dNg5jnATmdajE1mnQ',
  authDomain: 'ss-team3.firebaseapp.com',
  projectId: 'ss-team3',
  storageBucket: 'ss-team3.appspot.com',
  messagingSenderId: '843376149679',
  appId: '1:843376149679:web:797f63407c0bece380405a',
  measurementId: 'G-PSRXRHG6VR',
}

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig)
} else {
  firebase.app()
}

const googleAuthProvider = new firebase.auth.GoogleAuthProvider()

export const auth = firebase.auth()
export const signInWithGoogle = async () => auth.signInWithPopup(googleAuthProvider)
export const signOut = async () => { await auth.signOut() }
