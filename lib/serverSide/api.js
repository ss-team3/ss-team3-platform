import fs from 'fs'
import path from 'path'

import { cloneDeep } from 'lodash'

/**
 * @param {String} filePath: mock data file path (relative to ./data/)
 */
function getMockData (relativePath) {
  const filePath = path.join(process.cwd(), 'public', 'data', relativePath)

  const raw = fs.readFileSync(filePath)
  const data = JSON.parse(raw)
  return data
}

// Export mock api below
export async function getPosts (req, res) {
  const data = getMockData('Article/articles.json')
  return shuffleArray(data)
}
export async function getCategories (req, res) {
  return getMockData('Category/categories.json')
}

export async function getArticles (req, res) {
  const data = getMockData('Article/articles.json')
  data.sort(function (a, b) {
    return b.estimated_time - a.estimated_time
  })
  return data
}

export async function getPet (req, res) {
  return getMockData('Pet/pet.json')
}

export async function getUser (req, res) {
  return getMockData('User/user.json')
}

export async function getOwnedProducts (req, res) {
  return getMockData('OwnedProduct/ownedProducts.json')
}

// utils
export function shuffleArray (arr) {
  if (!Array.isArray(arr)) {
    console.error(`Args for 'shuffleArray' should be 'Array' type, but get ${typeof arr}`)
    return arr
  }
  const data = cloneDeep(arr)

  for (let i = data.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [data[i], data[j]] = [data[j], data[i]]
  }
  return data
}
